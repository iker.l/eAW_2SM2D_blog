---
title: HTML eta CSS baliabideak
subtitle: 
date: 2021-11-26
draft: false
description: 
tags: ["HTML","CSS","Baliabideak","Txuletak","Laburpena"]
---

HTML5 eta CSSri buruzko iturri ofiziala eta nagusia [https://www.w3schools.com/](https://www.w3schools.com/).

HTML && CSS Editoreak nabigatzailean: [https://jsfiddle.net/](https://jsfiddle.net/), [https://codepen.io](https://codepen.io).

* HTML5 TAG CHEAT SHEET - Created by WebsiteSetup.org [Chule_1-HTML5.pdf](res/Chule_1-HTML5.pdf)
* HTML 5 TAG REFERENCE  - THE WORKING BRAIN  [Chule_2-HTML5.pdf](res/Chule_2-HTML5.pdf)
* CHEAT SHEET DISEÑO WEB CSS3 [Chule_3-CSS3.pdf](Chule_3-CSS3.pdf)
* CSS. Conceptos básicos - Ateneu popular 2.0 [Chule_4-CSS3.pdf](res/Chule_4-CSS3.pdf)

## HTML5 TAG CHEAT SHEET

![](res/Chule_1-HTML5-1.png)

## HTML 5 TAG REFERENCE

![](res/Chule_2-HTML5-1.png)

## CHEAT SHEET DISEÑO WEB CSS3 

![](res/Chule_3-CSS3-1.png)

![](res/Chule_3-CSS3-2.png)

## CSS. Conceptos básicos

![](res/Chule_4-CSS3-1.png)










