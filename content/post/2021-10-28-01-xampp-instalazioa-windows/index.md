---
title: XAMPP instalazioa Windows sistema eragilean
subtitle: Web zerbitzari eramangarria 
date: 2021-10-28
draft: false
description: Web zerbitzari eramangarria 
tags: ["Instalazioa","Windows","XAMPP","Zerbitzaria","Web","HTTP"]
---

[XAMPP](https://www.apachefriends.org/es/about.html) software libreko paketea ([GNU GPL](https://eu.wikipedia.org/wiki/GNU_Lizentzia_Publiko_Orokorra) lizentzia), batez ere MySQL datu-baseak kudeatzeko sistema, [Apache](https://eu.wikipedia.org/wiki/Apache_HTTP_zerbitzaria) web zerbitzaria eta PHP eta Perl _script_ lengoaietarako interpreteak. Izena, berez, akronimoa da: x (edozein sistema eragiletarako), [Apache](https://eu.wikipedia.org/wiki/Apache_HTTP_zerbitzaria) , MariaDB/[MySQL](https://eu.wikipedia.org/wiki/MySQL), [PHP](https://eu.wikipedia.org/wiki/PHP), Perl. 5.6.15 bertsiotik aurrera, XAMPP-k MySQL datu-basea aldatu zuen MariaDB-gatik, MySQL-ren fork bat GPL lizentziarekin.

Software hau webguneak diseinatu eta konpontzeko web zerbitzari laguntzaile gisa erabiltzen da. XAMPP-ekin, web garatzaileek erraz probatu ditzakete web orriak Internetera konektatu beharrik izan gabe.

## XAMPP nola instalatu

Esteka hau erabili deskargatzeko [XAMPP download | SourceForge.net](https://sourceforge.net/projects/xampp/), tutorial hau idazteko unean fitxategia hau da: "xampp-windows-x64-7.4.25-0-VC15-installer.exe". 

Fitxategia deskargatu ondoren, exekutatu instalazioa has dadin, erabiltzailearen kontuen kontrola ([UAC](https://docs.microsoft.com/es-es/windows/security/identity-protection/user-account-control/how-user-account-control-works)) saihesteko, "C:\xampp" helbidean instalatuko dugu.

![](img/05.PNG)

Aurrera egin:

![](img/06.PNG)

Instalatu lehenetsitako osagai guztiak:

![](img/07.PNG)

XAMPP instalatzeko karpeta aukera dezakezu. Onena da proposatzen duen ibilbidea erabiltzea "C:\xampp".

![](img/08.PNG)

Ingelesera eta Alemanera baino ez dago itzulita, Ingelesa aukeratuko dugu.

![](img/09.PNG)

![](img/10.PNG)

![](img/11.PNG)

![](img/12.PNG)

Instalazioa bukatzen denean aginte panela exekutatutko dugu:

![](img/13.PNG)


## Aginte panela lehenbiziko aldiz

Aginte panela lehen aldiz exekutatzen dugunean:

![](img/14.PNG)

XAMPP kontrol-panela hiru eremutan banatzen da:

* **Moduluen eremua**, XAMPP modulu bakoitzerako adierazten duena: zerbitzu gisa instalatuta dagoen, haren izena, prozesuaren identifikatzailea, erabilitako portua eta botoi batzuk ditu prozesuak hasteko eta gelditzeko, administratzeko, konfigurazio-fitxategiak editatzeko eta jarduera-erregistroko fitxategiak irekitzeko.
* **Jakinarazpen-eremua**, non XAMPP-k egindako ekintzen arrakastaren edo porrotaren berri ematen duen.
* **Aplikazio erabilgarrien gunea**, azkar sartzeko.

Jarrian Apache, MySQL **zerbitzuak martxan jarriko ditugu**, baimenduko duen elkarrizketa-koadro bat agertuko da aplikazioa suebakiaren bidez. Egin klik Baimendu sarbidea botoia.

![](img/15.PNG)

Windows-en suebakian erantsitako arau berriak ikus daitezke (bat protokolo bakoitzerako, TCP edo UDP). Win + R eta idatzi `control firewall.cpl` zabaltzeko.

![](img/22.PNG)

"Apache HTTP Server" eta "mysqld":

![](img/23.PNG)

Dena ondo ondo joan dela egiaztatzeko zabaldu hurrengo helbidea [http://localhost/](http://localhost/) (edo [http://127.0.0.1/](http://127.0.0.1/)) edota sakatu "Apache" ondoan dagoen "Admin" botoiari.

![](img/16.PNG)

XAMPP lehenetsitako orrialdetik, egin klik gainean [phpinfo menuaren barratik](http://localhost/dashboard/phpinfo.php) PHPren xehetasun eta informazio guztiak ikusteko.

![](img/17.PNG)


phpMyAdmin web aplikazioan ere sartu [http://localhost/phpmyadmin/](http://localhost/phpmyadmin/) MySQL ondo dagoela baieztatzeko.

![](img/19.PNG)

Kontrol-panela exekutatu behar baduzu erabili: "c:\xampp\xampp-control.exe". Lasterbidea zuzena sor dezakezu mahaigainean, kontrol-panelera azkarrago sartzeko.

Zerbitzuak banan-banan abiarazteko edo gelditzeko:

* Apache & MySQL start: "c:\xampp\xampp_start.exe".
* Apache & MySQL stop: "c:\xampp\xampp_stop.exe".
* Apache start: "c:\xampp\apache_start.bat".
* Apache stop: "c:\xampp\apache_stop.bat".
* MySQL start: "c:\xampp\mysql_start.bat".
* MySQL stop: "c:\xampp\mysql_stop.bat".

### Zerbitzuak instalatu sistema eragilean

Zerbitzari bat zerbitzu gisa instalatu nahi badugu (ez da derrigorrezkoa), hau da, ordenagailua abiarazten dugun bakoitzean martxan jarri nahi badugu, dagokion Service laukia markatu behar da. Horretarako, XAMPP hasi behar da administratzaile gisa.

![](img/24.PNG)

Win+R eta sartu `services.msc` Windows-eko zerbitzuak ikuskatzeko.

### Aplikazio erabilgarriak

#### Netstat

Netstat (network statistics) konputagailu baten sareko konexio aktiboen zerrenda bat erakusten duen tresna da. Apache eta MySQL zerbitzarien funtzionamenduari buruz zalantzarik badugu, portuak irekita daudela egiazta dezakegu. Apache Web zerbitzariak (httpd.exe) 80. portua erabiltzen du normalean (aldatu daiteke) eta MySQL-k (mysqld.exe) 3306. portua.

![](img/18.PNG)

#### Shell

Agindu guztiak testu bidez egiten dira, komando-lerro baten bidez. 

![](img/20.PNG)

Hemendik ere MySQL zerbitzura sartu gaitezke (phpMyAdmin erabili gabe), instalatu berri dagoenean erabiltzailea "root" da eta ez du pasahitzik.

```bash
# mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 41
Server version: 10.4.21-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
5 rows in set (0.001 sec)

MariaDB [(none)]>
``` 

## Ezarpen nagusiak

### Aginte panela

"Config" botoia sakatu ezarpenen lehioa zabaltzeko.

### Apache

**Apache zerbitzuak funtzionatzen duen portua aldatzea**.

Apache "Config" botoia sakatu, httpd.conf aukeratu editatzeko, bilatu "Listen" ireki berri den testu fitxategian eta nahi duzun portua jarri (8080 ere ohikoa izaten da). Apache zerbitzua geldiarazi eta berriro abiarazi, orain "http://localhost:8080/" zabaldu ondo doala frogatzeko.

![](img/21.PNG)

## XAMPP desinstalatu

Exekutatu "C:\xampp\uninstall.exe" desintalazioa hasteko.

![](img/01.PNG)

Sortu dituzun webguneak ere ezabatu nahi badituzu, sakatu "Yes":

![](img/02.PNG)

Prozesua martxan, desinstalatzaileak zure erregistrotik XAMPPko sarrera guztiak ezabatuko ditu eta XAMPPrekin sartutako zerbitzu batzuk desinstalatuko ditu.

![](img/03.PNG)

Amaiera:

![](img/04.PNG)


## Baliabideak

* [https://bitnami.com/stack/xampp](https://bitnami.com/stack/xampp).




