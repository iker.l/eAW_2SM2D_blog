---
title: Wordfence segurtasun plugin-a
subtitle: Segurtasuna WordPress-en
date: 2021-11-05
draft: true
description: Segurtasuna WordPress-en
tags: ["WordPress","WP","WordPress - Hastapena","WordPress - Pluginak","WordPress - Wordfence"]
---

![](img/02.png)

<!-- vscode-markdown-toc -->
* 1. [Instalazioa](#Instalazioa)
* 2. [Atalak](#Atalak)
	* 2.1. [Dashboard](#Dashboard)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

[Wordfence](https://es.wordpress.org/plugins/wordfence/) WordPress-en _plugin_-a web suebaki bat (_waf - Web application firewall_) da. Etengabe zure guneak dituen mehatxu gehienak geldiarazten ditu, hala nola eskaera maltzurrak, edo nahi gabe irekita egon daitezkeen edo berriki eguneratzeak aplikatu ez diren atzeko ateetan kodeak sartzea. Zure webgunea babesteko ezinbestekoak diren beste segurtasun-neurri batzuk ere baditu.

Segurtasun-plugin honek doako bertsio bat du, baina, horrez gain, funtzio aurreratuagoak dituen ordainketa-bertsio bat ere badu.

##  1. <a name='Instalazioa'></a>Instalazioa

"Pluginak | Gehitu berria" atalera jo eta sartu bilatzailean "Wordfence", instalatu botoia sakatu.

![](img/01.png)

**Plugin bat instalatzeko garaian beti kontutan izan azken eguneraketa, zure WP bertsioarekin konpatiblea den, erabiltzaileen puntuazioa eta eritziak, etab.**

Behin instalatuta "Aktibatu" egin behar duzu.

![](img/03.png)

Orain alboko menu nagusian elemento berri bat ikusiko duzu "Wordfence" deritzona.

![](img/04.png)

##  2. <a name='Atalak'></a>Atalak

###  2.1. <a name='Dashboard'></a>Dashboard

Hemen datu guztien laburpena eta egoera ikusiko dituzu.

![](img/05.png)









