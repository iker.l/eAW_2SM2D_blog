---
title: HTML Estekak
subtitle: Oinarrizko HTML
date: 2021-11-28
draft: false
description: Oinarrizko HTML
tags: ["HTML","HTML - Estekak"]
---

<!-- vscode-markdown-toc -->
* 1. [Estekak](#Estekak)
* 2. [Hiperestekak](#Hiperestekak)
* 3. [Tokiko estekak](#Tokikoestekak)
* 4. [Beste orri batean dagoen hiperesteka baten esteka](#Besteorribateandagoenhiperestekabatenesteka)
* 5. [Title atributua](#Titleatributua)
* 6. [Leiho berri bat irekitzen duen esteka](#Leihoberribatirekitzenduenesteka)
* 7. [Estekak posta elektronikoko hebideekin](#Estekakpostaelektronikokohebideekin)
* 8. [Artxibo bat deskargatzeko esteka](#Artxibobatdeskargatzekoesteka)
* 9. [Hiperesteka bat irudi baten bidez](#Hiperestekabatirudibatenbidez)
* 10. [Baliabideak](#Baliabideak)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

[https://jsfiddle.net/ikerlandajuela/7rb23dtL/latest](https://jsfiddle.net/ikerlandajuela/7rb23dtL/latest)

[HTML_estekak.pdf](img/HTML_estekak.pdf)

##  1. <a name='Estekak'></a>Estekak

![](img/HTML_estekak/HTML_estekak-01.png)

Estekak `<a>...</a>` erabilita sortzen dira.

Atributu bat eramango du. 

```html
<a href = " " >
```

href balioak esaten dio estekari nora joan behar duen

![](img/HTML_estekak/HTML_estekak-02.png)

![](img/HTML_estekak/HTML_estekak-03.png)

**Adibidea**:

* Gorputzaren atalean lotura bat sortu. Horretarako, erantsi `<a>` etiketa. Adieraz ezazu zure estekan webgune bat, href atributuari balioa ezarriz.
* Zure loturaren deskribapena gehitu. Itxi elementua itxierako `</a>` etiketa erabilita.

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Estekak</title>  
  </head>
  <body>
  
    <a href="https://duckduckgo.com/"> DuckDuckGo - pribatasuna errespetatzen duen bilatzailea</a>
    
 
  </body>
</html>
```

![](img/HTML_estekak/HTML_estekak-04.png)

Bilatzaileak bisitatzen dituzte (kontuan hartu) (gogoratu ere ezarpenetan nola aldatu genituen esteka iraunkorrak irakurtzeko errezagoak izateko eta bilatzaileei lana errezteko).

Google eta beste batzuek web orrialdeetatik pasatzen dira loturak bilatzen, eta bat aurkitzen dutenean, orrialdea aipatzen duten hitzekin batera, helbidea gehitzen dute.

![](img/HTML_estekak/HTML_estekak-05.png)

Lotura deskribatzen duen testua erabili. Zaindu zure esteken testua. Ez egin hauek bezalako gauzak.

Ez duelako inolako zentzurik web orria atera nahi izatea norbaitek “Aquí” bilatzen duenean. “Deducción natural” bilatzen denean agertu beharko litzateke gure web orria bilatzailean.

![](img/HTML_estekak/HTML_estekak-06.png)

**Ez zehaztu loturaren helbururik (target).**

Ez duzu esan behar nola ireki behar zaion erabiltzaileari esteka bakoitza; berak inork
baino hobeto daki nola ireki nahi dituen une bakoitzean (pestaña batean, leiho berrian,
popup bezala, orrialde berean,...), eta jakingo du nola administratu.

Bisitaria hasiberria bada eta hori egiten ez badaki, gerta dakiokeen onena zera da: lotura bakoitza leku berean zabaltzea, beraz, zuk jarri lotura normala eta listo.

![](img/HTML_estekak/HTML_estekak-07.png)

##  2. <a name='Hiperestekak'></a>Hiperestekak

**Barne-loturak:** Hiperesteka baten esteka.

[https://jsfiddle.net/ikerlandajuela/mrtdLh3f/latest](https://jsfiddle.net/ikerlandajuela/mrtdLh3f/latest).

Hiperesteka bat erreferentziazko puntu moduko bat da, eta HTML orrietan sar daitezke oso luzeak direnean. Izan ere, baliagarriak izan daitezke orrialde berean beherago dagoen puntu batera lotura bat sortzeko, bisitariak interesatzen zaien tokira zuzenean salto egin ahal izan dezaten.

![](img/HTML_estekak/HTML_estekak-08.png)

Hiperesteka berri bat sortzeko, edozein etiketa izan daiteke, eta id atributua erabiliko dugu. Adibidez, titulu bat.

```html
<h2 id ="hiperesteka1">Titulua</h2>
```

Orain, lotura bat sortu besterik ez duzu egin behar, baina oraingo honetan href atributuak
almohadilla bat (#) izango du eta ondoren loturaren izena. Adibidea:

```html
<a href="#hiperesteka1">Hiperestekara joan</a>
```

![](img/HTML_estekak/HTML_estekak-09.png)

Normalean, loturan klik egiten baduzu, orrialde berean beherago dagoen puntu batera eramango zaitu (betiere, orrialdeak
behar besteko testua badu desplazamendu barrak automatikoki
mugitzeko). 

Hiperestekak erabiltzen dituen testu asko duen orri baten adibide bat da, (testu asko jartzen duen tokietan idatzi testu asko):

![](img/HTML_estekak/HTML_estekak-10.png)

![](img/HTML_estekak/HTML_estekak-11.png)

##  3. <a name='Tokikoestekak'></a>Tokiko estekak

Web guneko beste orri batzuetara doazenak eramaten gaituztenak dira.

![](img/HTML_estekak/HTML_estekak-12.png)

```html
<a href="fitxategia.html">....</a>
```

Web gunean artxiboa non dagoen kontuan izan behar dugu.

![](img/HTML_estekak/HTML_estekak-13.png)

![](img/HTML_estekak/HTML_estekak-14.png)
##  4. <a name='Besteorribateandagoenhiperestekabatenesteka'></a>Beste orri batean dagoen hiperesteka baten esteka


Esteka bat sortzea da asmoa, honek orri berri bat zabalduko du eta beherago dagoen hiperesteka batera eramango zaitu.

Praktikan oso erraza da egitea: orriaren izena idatzi, gero almohadilla bat (#), eta ondoren hiperestekaren izena.

![](img/HTML_estekak/HTML_estekak-15.png)

Adibidea:

```html
<a href="2orria.html#deskribapena">
```

2orrira.html orrira eramango zaitu, zuzenean deskribapena hiperestekara.

Orri honek hiru esteka ditu, horietako bakoitzak anchors.html orriko hiperesteka ezberdinetara eramaten duelarik.

![](img/HTML_estekak/HTML_estekak-16.png)

![](img/HTML_estekak/HTML_estekak-17.png)

##  5. <a name='Titleatributua'></a>Title atributua

`title` atributua erabiliko da kurtsorea loturaren gainean jartzean goruntz egiten duen deskripzioa erakusteko. Atributu hau aukerakoa da.

![](img/HTML_estekak/HTML_estekak-18.png)

Deskripzioa baliagarria izan daiteke erabiltzaileei informazioa emateko, baita lotura egin aurretik ere. Hona hemen emaitza hori lortzeko modua:

![](img/HTML_estekak/HTML_estekak-19.png)

##  6. <a name='Leihoberribatirekitzenduenesteka'></a>Leiho berri bat irekitzen duen esteka

Lotura batek leiho berri bat irekitzea “behartu” dezake. Hau egiteko, `target="_blank"` gehitzen zaio `<a>` etiketari.

![](img/HTML_estekak/HTML_estekak-20.png)

##  7. <a name='Estekakpostaelektronikokohebideekin'></a>Estekak posta elektronikoko hebideekin

Zure bisitariek posta elektroniko bat bidali ahal izateko, mailto motako loturak erabil ditzakezu. Etiketa ez da ezer aldatzen. Atributuaren balioa bakarrik aldatzen da:

```html
<a href="mailto:izena@email.com">Bidali mezu elektroniko bat!</a>
```

![](img/HTML_estekak/HTML_estekak-21.png)

Gomendioa: posta helbideetara estekak jartzen dituzunean, idatz ezazu loturaren edukian (`<a>` eta `</a>` artean) idatzi
beharreko posta-helbidea. Izan ere, erabiltzaileak ez badu postaprograma bat konfiguratu ordenagailuan, ezin izango du mezurik bidali, baina, gutxienez, posta elektronikoko helbidea kopiatu eta beste ordenagailu baten edo Web-mail sistema baten bidez idatzi ahal izango du posta.

![](img/HTML_estekak/HTML_estekak-22.png)

##  8. <a name='Artxibobatdeskargatzekoesteka'></a>Artxibo bat deskargatzeko esteka

Artxibo bat deskargatzeko, web orri bat lotzeko erabili dugun prozedura berdina jarraitu behar digu, baina oraingo honetan artxiboaren izena adierazi behar dugu.

![](img/HTML_estekak/HTML_estekak-23.png)

Adibidez, imajinatu myfile.zip deskargatu nahi dituzula. Artxibo hau zuen web orria (edo azpikarpeta batean) dagoen karpeta berean jarri behar duzu, eta fitxategi honetara esteka bat sortu:

![](img/HTML_estekak/HTML_estekak-24.png)

##  9. <a name='Hiperestekabatirudibatenbidez'></a>Hiperesteka bat irudi baten bidez

Hiperesteka bat ezar dezakegu, baina testu bat erakutsi beharrean, irudi bat jarriko dugu.

Honetarako `<img>` marka jarri beharko dugu estekaren hasierako eta amaierako markaren artean (`<a>`).

![](img/HTML_estekak/HTML_estekak-25.png)

Bi irudi (foto1.jpg eta foto2.jpg) hiperesteka gisa erakusten dituen orri bat sortuko dugu. Klik egitean beste orri
batera deituko dute. Irudiak web orria dagoen toki berean dagoen karpeta batean daude (karpetak Irudiak du izena).

![](img/HTML_estekak/HTML_estekak-26.png)

![](img/HTML_estekak/HTML_estekak-27.png)

##  10. <a name='Baliabideak'></a>Baliabideak

* []