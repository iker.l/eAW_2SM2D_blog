---
title: Ohiko terminoak WordPress-en
subtitle: WordPress hastapenak
date: 2021-10-30
draft: false
description: WordPress hastapenak
tags: ["WordPress","WP","WordPress - Hastapena","Wordpress - Hiztegia","Terminologia"]
---

## Ohiko terminoak ulertzen 

Blogging-munduan berria bazara (batzuetan _blogosphere_ deitzen zaio, egun hauetan nahiko ezaguna den esamoldea), hitz arrunt hauek ezagutu nahi dituzu:

### Post edo bidalketak

**Blogeko sarrera bakoitzari _post_ bat esaten zaio**. Post bakoitzak, normalean, hainbat zati ditu. Jakina, bi zati argienak izenburua eta edukia dira. Edukia izan daitezke testua, irudiak, estekak, etab. Multimedia ere izan dezakete (adibidez, bideoak eta audio-fitxategiak). Post bakoitzak argitalpen data bat eta autore bat ere badu, eta gehienek **kategoria eta etiketa** bat edo gehiago dituzte esleituta.

### Kategoriak eta etiketak

**Blogetan argitalpenak antolatu eta aurkitzeko moduak dira**. **Kategoriak** gaiak bezala dira, eta **etiketak** gako hitzen antzekoak. Adibidez, janariari eta sukaldaritzari buruzko blog baterako, errezeta izeneko kategoria dago, baina kategoria horretako post bakoitzak hainbat etiketa izan ditzake (adibidez, zopa, begetarianoa eta esnerik gabea).

### Iruzkinak

Blog gehienek **bidalketei buruzko iruzkinak argitaratzeko aukera ematen diete bisitariei**. Horrek aukera ematen die irakurleei blogaren egilearekin elkarreraginean aritzeko, horrela esperientzia interaktibo guztia eginez. Askotan, blogaren egileak iruzkinei erantzun behar die iruzkin osagarriak argitaratuz, erantzuteko botoian klik bakar bat eginez. Botoi hori etengabeko elkarrizketarako edo elkarrizketarako erabiltzen da.

### Gaiak

**Gaia zure blogerako aukeratu dezakezun diseinu estetikoa da**. Blog gehienetan, edukia (adibidez, post-ak) ikusmenetik bereizita dago. Horrek esan nahi du zure blogeko irudiak noiznahi alda ditzakezula, kezkatu gabe edukiari eragiten bazaio. Gaiei buruzko gauzarik onenetako bat da minutu batzuk besterik ez duela hartzen instalatzeko eta berria erabiltzen hasteko. Gainera, oso gai onak daude doan edo kostu txikikoak.

[https://wordpress.org/themes/](https://wordpress.org/themes/)

![](img/01.PNG)

## Plugin-ak

WordPress-en plugin-ak web-softwarearen pieza txikiak dira, eta WordPress-en edozein lekutan instala daitezke. Jatorrizko funtzionaltasuna handitzen dute, gaur egungo teknologiak onartzen duen ia guztia egiteko.

WordPress-en web gune edo blog bakoitzak plugin-kopuru mugagabearekin lan egin dezake. Pluginen bidez sartutako funtzionalitate ezagunenak hauek dira: spamaren babesa, bilaketa-motorren optimizazioa (SEO), cachea, sare sozialen integrazioa, segurtasun-kopiak, etab.

[https://es.wordpress.org/plugins/](https://es.wordpress.org/plugins/).

![](img/02.PNG)


* [https://wordpress.org/support/article/managing-plugins/](https://wordpress.org/support/article/managing-plugins/)

## Widget-ak

Widget-en erabilera arruntena da bere lekuko alboko barretan erakustea. Oro har, zure egungo gaiak widget-eremu batzuk emango dizkizu, eta horietan widget-ak erakuts ditzakezu (esan bezala, horietako asko alboan daude). Widget-en ohiko erabileretako batzuk edukiak erakusteko dira, hala nola kategoriak eta etiketak, argitalpen berriak, herri-argitalpenak, azken iruzkinak, artxibatutako postuetarako estekak, orriak, estekak, bilaketa-eremuak edo formateatu gabeko testu estandarra.

* [https://wordpress.org/support/article/wordpress-widgets/](https://wordpress.org/support/article/wordpress-widgets/).

## Menuak


## Orrialdeak

**Garrantzitsua da orrialdearen eta argitalpenaren arteko aldea ulertzea**. Sarrerak (_post_) ez bezala, orrialdeak ez daude denbora-marken mende, eta ez dira alderantzizko ordena kronologikoan agertzen. Ez dute ez kategoriarik ez etiketarik. Orrialde bat izenburu bakarra duen edukia da, eta edukia. Litekeena da zure blogeko orrialde-kopurua nahiko estatikoa izatea; posts berriak, berriz, gero eta gehiago edo gutxiago gehitu daitezke.

## Azaleko orria

Hasierako orri bat bisitariek web gunea bisitatzen dutenean ikusten duten orri nagusia besterik ez da (_home_ deritzona), beren domeinu-izenean edo URL helbidean idatzita (adibidez [http://localhost/wordpress](http://localhost/wordpress) sartzen zarenean). WordPress sortu zen lehen egunetan, orrialde nagusi bat ez zen aparteko orrialde mota gisa hitz egiten genuena. Hasiera batean, hasierako orri bat automatikoki sortu zen sarrera berrietatik abiatuta: alderantzizko ordena kronologikoan zegoen lanpostu horien zerrenda. Gaur egun, ordea, WordPress-ek aukera ematen digu hasierako orri guztiz pertsonalizatu bat eraikitzeko eta bertan nahi dugun edozein eduki erakusteko.

## Erabiltzaileak

WordPress-en ezaugarrietako bat da gai dela hainbat erabiltzaile-konturekin lan egiteko, ez bakarrik gunearen jabearena den kontu bakar batekin (administratzailea/autore nagusia). Erabiltzaile-kontu mota desberdinak daude, eta **hainbat egiaztagiri eta sarbide-eskubide dituzte**.

![](img/03.jpg)

* [https://wordpress.org/support/article/roles-and-capabilities/](https://wordpress.org/support/article/roles-and-capabilities/).


