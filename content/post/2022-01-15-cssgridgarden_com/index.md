---
title: GRID GARDEN
subtitle: CSS Grid jokoa
date: 2022-01-15
draft: false
description: HTML5 eta CSS3
tags: ["HTML","CSS","CSS - Grid","Grid"]
---

## 01

![](img/01.png)

Erantzuna:

```css
grid-column-start: 3;
```

Erabili [`grid-column-start`](https://www.w3schools.com/cssref/pr_grid-column-start.asp) elementu bat non hasten den adierazteko.

Adibidea: [src/01.html](src/01.html)

![](img/02.png)

---

## 02

![](img/03.png)

Erantzuna:

```css
grid-column-start: 5;
```

---

## 03

![](img/04.png)

Erantzuna:

```css
grid-column-end: 4;
```

[`grid-column-end`](https://www.w3schools.com/cssref/pr_grid-column-end.asp) esaten du zenbat zutabeetan zabalduko den elementu bat, edo ze zutabeetan bukatuko den.

Adibidea: [src/03.html](src/03.html)

--- 

## 04

![](img/05.png)

Erantzuna:

```css
grid-column-end: 2;
```

---

## 05

![](img/06.png)

Erantzuna:

```css
grid-column-end: -2;
```

---

## 06

![](img/07.png)

Erantzuna:

```css
grid-column-start: -3;
```

---

## 07

![](img/08.png)

Erantzuna:

```css
grid-column-end: -3;
grid-column-end: span 2;
```

[W3Schools: CSS grid-column-end Property](https://www.w3schools.com/cssref/pr_grid-column-end.asp).

Adibideak:

* [src/08-01.html](src/08-01.html)

---

## 08

![](img/09.png)

Erantzuna:

```css
grid-column-end: span 5;
```

---

## 09

![](img/10.png)

Erantzuna:

```css

```

---

















