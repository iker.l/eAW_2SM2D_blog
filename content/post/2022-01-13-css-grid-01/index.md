---
title: CSS Grid (01)
subtitle: HTML5 eta CSS3
date: 2022-01-13
draft: false
description: HTML5 eta CSS3
tags: ["HTML","CSS","CSS - Grid","Grid"]
---

<!-- vscode-markdown-toc -->
* 1. [Grid elementuak](#Gridelementuak)
* 2. [Display ezaugarria](#Displayezaugarria)
* 3. [Zutabeak](#Zutabeak)
* 4. [Lerroak](#Lerroak)
* 5. [Gaps](#Gaps)
* 6. [Grid Lines](#GridLines)
* 7. [Oinarrizko adibideak](#Oinarrizkoadibideak)
	* 7.1. [display](#display)
	* 7.2. [grid-template](#grid-template)
	* 7.3. [grid-gap](#grid-gap)
* 8. [Elementuak lerrokatu](#Elementuaklerrokatu)
* 9. [Kanpo estekak](#Kanpoestekak)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

![](img/05.png)

CSS Grid Layout moduluak sarean oinarritutako diseinu-sistema bat eskaintzen du, **errenkada eta zutabeekin**. Horri esker, errazagoa da web-orriak diseinatzea, _float_ eta posizionamendua erabili beharrik gabe. Ez da _flex_-en alternatiba; gainera, _grid_ sistema eta _flex_ batera konbina daitezke.

![](img/24.jpg)

[CSS Grid gero eta ezagunagoa da](https://chromestatus.com/metrics/css/timeline/popularity/453) eta 

##  1. <a name='Gridelementuak'></a>Grid elementuak

_grid_ baten diseinua elementu aita bat da, haurrentzako elementu bat edo gehiago dituena.

```css
.grid-container {
  display: grid;    
}
```

[src/01.html](src/01.html)

![](img/01.png)

Sare osoaren altuera eta zabalera finkatu dezakegu:

```css
.grid-container {
  display: grid;    
  width: 400px;
  height: 200px;
}
```

![](img/25.png)

[`grid-template-columns`](https://developer.mozilla.org/es/docs/Web/CSS/grid-template-columns) CSS ezaugarriak lerroen izenak ezartzen ditu, honekin 3 zutabez osatutako sare edukiontzia sortu dugu (w3schools: [CSS grid-template-columns Property](https://www.w3schools.com/cssref/pr_grid-template-columns.asp)).

![](img/03.png)

```css
.grid-container {
  display: grid;
  grid-template-columns: auto auto auto;
}
```

```html
<div class="grid-container">
  <div class="grid-item">1</div>
  <div class="grid-item">2</div>
  <div class="grid-item">3</div>  
  <div class="grid-item">4</div>
  <div class="grid-item">5</div>
  <div class="grid-item">6</div>  
  <div class="grid-item">7</div>
  <div class="grid-item">8</div>
  <div class="grid-item">9</div>  
</div>
```

[src/02.html](src/02.html)

![](img/02.png)


Zubateak `grid-template-columns: auto auto auto;` jarri beharrean zabalera bat zehaztu ahal dugu:

```css
.grid-container {
  display: grid;
  grid-template-columns: 80% 10% 10%;
  background-color: #2196F3;
  padding: 10px;  
}
```

[src/02.html](src/06.html)

![](img/10.png)


##  2. <a name='Displayezaugarria'></a>Display ezaugarria

HTML elementu bat _grid_ edukiontzi bihurtzen da, `display` propietatea `grid` edo `inline-grid` bezala doituta dagoenean.

```css
.grid-container {
  display: grid;
}
```

edo 

```css
.grid-container {
  display: inline-grid;
}
```

Adibidea `display: inline-grid;` erabilita, [`inline-block`](https://www.w3schools.com/css/css_inline-block.asp)-aren baliokidea da, desberdintasun nagusia pantaila da: `display: inline-block` ez du lerro-haustura bat gehitzen elementuaren ondoren, eta, beraz, elementua beste elementu batzuen ondoan eser daiteke (w3schools: [CSS Layout - display: inline-block](https://www.w3schools.com/css/css_inline-block.asp)). 

[src/03.html](src/03.html)

![](img/04.png)

##  3. <a name='Zutabeak'></a>Zutabeak

![](img/06.png)

##  4. <a name='Lerroak'></a>Lerroak

![](img/07.png)

##  5. <a name='Gaps'></a>Gaps

Zutabe/errenkada bakoitzaren arteko espazioei hutsune edo _gap_ deritze.

![](img/08.png)

Hutsune edo _gap_-aren tamaina doi dezakezu propietate hauetako bat erabiliz:

* [`grid-column-gap`](https://developer.mozilla.org/es/docs/Web/CSS/column-gap)
* [`grid-row-gap`](https://developer.mozilla.org/en-US/docs/Web/CSS/row-gap)
* [`grid-gap`](https://developer.mozilla.org/es/docs/Web/CSS/gap)

`grid-column-gap: 50px;` erabili dut adibide honetan `.grid-container` klasearen propietate bezala.

[src/04.html](src/04.html)

![](img/09.png)


```css
.grid-container {
  display: grid;
  grid-row-gap: 50px;
  grid-template-columns: auto auto auto;
  background-color: #2196F3;
  padding: 10px;
}
```

`grid-row-gap: 50px;` erabili dut adibide honetan:

[src/07.html](src/07.html)

![](img/11.png)

[`grid-gap`](https://developer.mozilla.org/es/docs/Web/CSS/gap) propietatea aurreko bien laburdura da, adibidez:

```css
.grid-container {
  display: grid;
  grid-gap: 50px 100px;
}
```

[src/08.html](src/08.html)

![](img/12.png)

Balore bakarra ezarri daiteke bientzako horrela:

```css
.grid-container {
  display: grid;
  grid-gap: 50px;
}
```

##  6. <a name='GridLines'></a>Grid Lines

Zutabeen arteko lerroei zutabe-lerroak deitzen zaie.

Lerroen arteko lerroei errenkada deritze.

![](img/13.png)

Jarri sareko elementu bat zutabearen 1. lerroan, eta utzi zutabearen 3. lerroan amaitzen:

```css
.item1 {
  grid-column-start: 1;
  grid-column-end: 3;
}
```

[src/09.html](src/09.html)

![](img/14.png)

Jarri sareko elementu bat 1. lerroan, eta utzi 3. lerroan amaitzen:

```css
.item1 {
  grid-row-start: 1;
  grid-row-end: 3;
}
```

[src/10.html](src/10.html)

##  7. <a name='Oinarrizkoadibideak'></a>Oinarrizko adibideak

###  7.1. <a name='display'></a>display

![](img/15.png)

```css
display: grid;
```

![](img/16.png)

```css
display: inline-grid;
```

###  7.2. <a name='grid-template'></a>grid-template

![](img/17.png)

```css
grid-template-columns: 12px 12px 12px;
grid-template-rows: 12px 12px 12px;
```

![](img/18.png)

```css
grid-template-columns: 8px auto 8px;
grid-template-rows: 8px auto 12px;
```

![](img/19.png)

```css
grid-template-columns: 22% 22% auto;
grid-template-rows: 22% auto 22%;
```

![](img/20.png)

```css
grid-template-columns: repeat(3, 12px);
grid-template-rows: repeat(3, auto);
```

###  7.3. <a name='grid-gap'></a>grid-gap

![](img/21.png)

```css
grid-row-gap: 1px;
grid-column-gap: 9px;
```

![](img/22.png)

```css
grid-gap: 1px 9px;
```

![](img/23.png)

```css
grid-gap: 6px;
```

##  9. <a name='Kanpoestekak'></a>Kanpo estekak

* MDN Web Docs [Basic concepts of grid layout](https://developer.mozilla.org/es/docs/Web/CSS/CSS_Grid_Layout/Basic_Concepts_of_Grid_Layout).
* [https://developer.mozilla.org/es/docs/Web/CSS/CSS_Grid_Layout](https://developer.mozilla.org/es/docs/Web/CSS/CSS_Grid_Layout)

Beste iturri batzuk:

* [https://yoksel.github.io/grid-cheatsheet/](https://yoksel.github.io/grid-cheatsheet/).

Frameworks:

* [https://960.gs/](https://960.gs/).
* [https://getbootstrap.com/](https://getbootstrap.com/).
* [http://www.blueprintcss.org/](http://www.blueprintcss.org/): Blueprint is a CSS framework, which aims to cut down on your development time. It gives you a solid foundation to build your project on top of, with an easy-to-use grid, sensible typography, useful plugins, and even a stylesheet for printing.
* [https://get.foundation/](https://get.foundation/).