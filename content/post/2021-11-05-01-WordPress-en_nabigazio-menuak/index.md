---
title: WordPress-en nabigazio-menuak
subtitle: Pertsonalizatu zure lekuaren diseinua eta itxura
date: 2021-11-05
draft: false
description: Pertsonalizatu zure lekuaren diseinua eta itxura
tags: ["WordPress","WP","WordPress - Hastapena","WordPress - Menuak"]
---

<!-- vscode-markdown-toc -->
* 1. [Sarrera](#Sarrera)
* 2. [Menu bat gehituz](#Menubatgehituz)
* 3. [Menu-maila ugari sortuz](#Menu-mailaugarisortuz)
* 4. [Menu bat bistaratzea](#Menubatbistaratzea)
* 5. [Menuko elementu bat ezabatzen](#Menukoelementubatezabatzen)
* 6. [Propietate aurreratuak](#Propietateaurreratuak)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Sarrera'></a>Sarrera

Menuak oso garrantzitsuak dira web gunean errazago nabigatzeko eta informazioa bilatzeko. Menu klasikoak orriaren goiko aldean kokatzen dira, web gunea osatzen duten banakako orriak zerrendatuz. Orri tipiko batzuk harremanetarako dira, edo nor garen, adibidez. Funtsean, gure guneko hainbat eremutan izena ematen duten esteka-zerrendak dira.

WPk menuak modu lagunkoian kudeatzen ditu; erraz sor daitezke **orrialde (_page_), bidalketa (_post_), kategoria jakin batzuk edo URL batera edozein esteka adierazten duten menuak**.

##  2. <a name='Menubatgehituz'></a>Menu bat gehituz

Idazmahaiaren barruan, **Itxura | Menuak** atalera joan behar dugu. Gure lehen menua sortzeko, **izen bat sartu eta aldaketak gorde** behar ditugu “Sortu menua” botoiarekin.

![](img/01.png)

Orain, ezkerreko paneletik **orri jakin** batzuk gehi ditzakegu (beste elementu batzuk sartu ditzazkegu menuko aukera bezala: esteka bat, bidalketa bat, kategoriak, etab...), “Gehitu menura” botoiarekin.

![](img/02.png)

Bidalketa zehatzak gehitu nahi badituzu, Bidalketak atala eta “Gehitu menura” botoia erabil ditzakezu.

Esteka pertsonalizatuak gehi daitezke URL batekin; azpimenuko beste elementu batzuk dituen eta inon nabigatzen ez duen menuko elementu bat gehitu nahi badugu, URL eremuan “#” ikurra erabiliko dugu.

![](img/03.png)

Kategoriak gehi ditzakegu menuko elementu gisa, kategoria horretako sarrera guztiak erakuts ditzan.

**Egiten ditugun aldaketak gorde behar ditugu beti.**

##  3. <a name='Menu-mailaugarisortuz'></a>Menu-maila ugari sortuz

Menuko elementu bat arrastatu eta askatu dezakegu, aitaren azpi elementu gisa kokatzeko. Gora eta behera ere arrastatu ditzakegu, lekuz aldatzeko.

![](img/04.png)

##  4. <a name='Menubatbistaratzea'></a>Menu bat bistaratzea

Instalatutako _theme_-aren arabera, hainbat kokapen izan ditzakegu gure menurako, eta, gainera, bikoiztuta eduki dezakegu zenbait lekutan (nahiz eta ez izan zentzu handirik).

![](img/05.png)

##  5. <a name='Menukoelementubatezabatzen'></a>Menuko elementu bat ezabatzen

Menutik elementu bat ezabatzea oso erraza da; elementuaren aukerak zabalduz gero, aukera aurkituko dugu.

![](img/06.png)

##  6. <a name='Propietateaurreratuak'></a>Propietate aurreratuak

Menuen pantailaren goiko aldean, **Pantaila aukerak** atala zabalduko dugu.

![](img/07.png)

**Izenburu atributua**: Azaltzen den testua sagua gainetik pasatzen dugunean.

**CSS klaseek** menuaren estilo gehigarriak definitzeko aukera ematen dute.

Fitxa berri batean elementua irekiaraz dezakegu:

![](img/08.png)

## Baliabideak

* [https://codex.wordpress.org/WordPress_Menu_User_Guide](https://codex.wordpress.org/WordPress_Menu_User_Guide).
* [https://wordpress.com/support/advanced-menu-settings/](https://wordpress.com/support/advanced-menu-settings/).
* [https://wpza.net/how-to-open-menu-link-in-a-new-tab-in-wordpress/](https://wpza.net/how-to-open-menu-link-in-a-new-tab-in-wordpress/).
