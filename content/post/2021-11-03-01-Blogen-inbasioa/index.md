---
title: Blogen inbasioa!
subtitle: Wordpress-en sorrera eta bizitza
date: 2021-11-03
draft: false
description: Wordpress-en sorrera eta bizitza
tags: ["WordPress","Mint","Zerbitzaria","VBox","Virtual Box"]
---

## Blogosfera eta komunikazioaren paradigma berria

Milurteko berriarekin blogak eta masa-autokomunikazioa sortzen dira (informazio-zerbitzuak pertsonalizatuz **komunikazio publikoa desmasifikatzeaz** ere hitz egiten da), **komunikazioaren [paradigma](https://www.euskaltzaindia.eus/index.php?sarrera=Paradigma&antzekoak=ez&option=com_hiztegianbilatu&view=frontpage&layout=aurreratua&Itemid=410&lang=eu-ES&bila=bai) berri bat sortzen da**, orain arte komunikabide tradizionalak gutxi batzuen esku zeuden (enpresa handiak, presio-_lobbiak_ eta, funtsean, gobernuak), **komunikazioa norabide bakarreko bide batera mugatzen zen**, non gizarteak modu pasiboan kontsumitzen baitzuen (irratia, telebista, prentsa, liburuak edo musika diskoak adibidez).

![](img/09.jpg)

Irudia: XV. mendeko Europako inprimategia ([Wikipedia iturria](https://es.wikipedia.org/wiki/Imprenta)), eskuz kopiak egitetik (erderaz [amanuense](https://es.wikipedia.org/wiki/Copista) bezala ezagutzen duguna) sistema mekaniko bat erabilita sortzen den paradigma.

**Blogen sorrerari** esker, pertsona “anonimoek” beraien etxeetatik edozein informazio eman dezakete, **“prentsa-askatasuna ez da jada prentsa duenarentzat”**; lehen aldiz, jendeak bere historia lehen pertsonan konta dezake, [_mass media_](https://es.wikipedia.org/wiki/Medio_de_comunicaci%C3%B3n_de_masas) handiek bezala, bitartekaririk gabe. **Herritarren kazetaritzaz** hitz egiten hasten da, kasu askotan kontakizun ofizialaren kontrakoak edo interes ilunen mende dauden kate handiak lotsatzen dituztenak.

**1998**an, mugarri bat gertatu zen sarean, [**Blogger**](https://es.wikipedia.org/wiki/Blogger) sortu zen, **lineako bitakora argitaratzeko lehen tresnetako bat**. Edukiak alderantzizko ordena kronologikoan erakusten dira (Wordpress edo beste edozein blog bezala). Erabiltzaileak ez du koderik idatzi behar, eta publiko bat ia berehala erabiliz; ordurako bazeuden [Slashdot](https://es.wikipedia.org/wiki/Slashdot) (1997) eta [LiveJournal](https://es.wikipedia.org/wiki/LiveJournal).

![](img/01.png)

Blogger, about page on Pyra website (screenshot February 2000) [Iturria](https://www.firstversions.com/2015/08/blogger.html).

Ordura arte, webguneak HTML ezagutzen zuten espezialisten gauza ziren, eta ostatu-zerbitzu bat kontrata zezaketenek. Une horretatik aurrera, **blogek eta wikiek** ([Wikipedia](https://eu.wikipedia.org/wiki/Wikipedia) 2001ean sortu zen, [GPL](https://eu.wikipedia.org/wiki/GNU_Lizentzia_Publiko_Orokorra) lizentziapean) **edukia modu errazean argitaratzeko aukera ematen dute**. Testu bat [WYSIWYG](https://eu.wikipedia.org/wiki/WYSIWYG) editore batean idatzi besterik ez da egin behar, gehienez jota formatuarekin bakarrik arduratu behar da, letra lodiekin eta izenburuekin, minutu gutxitan, edonork bere webgunea martxan jarri eta edukiak argitaratzen hasten da.

Denbora gutxian blogen "inbasioa" ematen da (The New York Times [Invasion of the ‘Blog’: A Parallel Web of Personal Journals](https://www.nytimes.com/2000/12/28/technology/invasion-of-the-blog-a-parallel-web-of-personal-journals.html)) elkarrekin sarekatuta, "zaleek" mantenduta, komunikazioan profesionalak ez direnak, imajina daitekeen edozein gauzaren berri ematen dutenak.

## WordPress-en sorrera eta bilakaera

![](img/08.png)

Iturria: [https://codex.wordpress.org/WordPress_Versions](https://codex.wordpress.org/WordPress_Versions).

[WordPress](https://eu.wikipedia.org/wiki/WordPress)-ek 2003. urtean ikusi zuen argia (GPL lizentzia duen [b2/cafelog](https://ma.tt/2003/01/the-blogging-software-dilemma/)-eko _fork_ bat), hasiera batean [CMS](https://eu.wikipedia.org/wiki/Edukiak_kudeatzeko_sistema) (edukiak kudeatzeko sistema) edo blog gisa zabaldu zen, baina ia edozein web mota sortzeko gai da gaur egun (online dendak, etab), [**GPL**](https://eu.wikipedia.org/wiki/GNU_Lizentzia_Publiko_Orokorra) gisa lizentzia ematen da eta **software librea** da; [**PHP**](https://eu.wikipedia.org/wiki/PHP) lengoaian garatuta dago, eta [HTTP Apache zerbitzari](https://eu.wikipedia.org/wiki/Apache_HTTP_zerbitzaria) batean exekutatzen da, normalean, datu-basea kudeatzeko sistema gisa, [MySQL](https://eu.wikipedia.org/wiki/MySQL) erabiltzen du (nahiz eta Nginx, MariaDB, PostreSQL eta beste teknologia askorekin lan egin dezakeen gaur egun).

Beheko hau da **WordPress-ek lehen bertsioan** ([v0.71, 2003ko maiatza](http://wordpress.org/news/2003/05/wordpress-now-available/)) duen _backend_-aren itxura_; zuzenean _post_-ak (bidalketak edo sarrerak) argitaratzeko formulario bat du, eta pixka bat gehiago, funtzionalitateak oso mugatuak dira, **post bati kategoria bakarra eslei dakioke**.

![](img/02.png)

2004ko maiatzeko [**1.2 bertsiora**](http://wordpress.org/news/2004/05/heres-the-beef/) arte ez dira sartzen **_pluginak_**. Horri esker, garatzaileek eta erabiltzaileek WordPress-en funtzionalitateak zabaldu eta, nahi izanez gero, partekatu egin ditzakete. **WordPress komunitateari irekitzen ari zaio**, _blogging_-aren industriako beste aplikatibo batzuk egiten ari direnaren aurkakoa.

2005eko otsaileko [**1.5 bertsioak**](http://wordpress.org/news/2005/02/strayhorn/) “Strayhorn” izena du ([Billy Strayhorn](https://es.wikipedia.org/wiki/Billy_Strayhorn) eta antzeko musikari-izenak erabiltzen dira), eta berrikuntza garrantzitsuak ditu; adibidez, **_theme_-sistema guztiz berria, malguagoa**; besterik adierazi ezean, orain [Kubrick](https://wordpress.org/themes/default/) izeneko _theme_ berri bat erabiltzen da, _smartphone_-ak oraindik ez dira zabaldu, eta _theme_-a ez da _responsive_.

Goiburua, oina edo alboko barrak bezalako elementuak PHP fitxategi propioetan gordetzen dira (diseinu modularra), eta horrek aldaketak egitea errazten du. URLak hobeto eta irakurgarriago bihurtzen dituzten _permalink_-ak sartzen dira (http://example.com/?p=N -> “http://example.com/2012/post-name/); Apache (mod_rewrite) aldatu behar da erabili ahal izateko.

![](img/03.png)

[2.0 bertsio](http://wordpress.org/news/2005/12/wp2/) berri batek ere, **2005ean**, administrazioa kontrolatzeko panel berri bat (_admin dashboard_) sartu zuen, eta **Javascript kodea ere erantsi zuen**, orria etengabe ez kargatzeko aldaketarik sinpleenetarako. Gehien erabiltzen den pluginetako bat ere aurrez instalatuta dago, eta [**Akismet**](https://akismet.com/) spamaren aurkako iragazkia da.

![](img/11.jpg)

[Duke Ellington](https://eu.wikipedia.org/wiki/Duke_Ellington) musikariren izena darama 2.0 bertsioak.

Hala, urteak igaro ahala, bertsio berriak hobetu eta funtzionalitate berriak sartzen dituzte, hobekuntzak eta funtzionalitate berriak dituztenak; [2.7 bertsioak (Coltrane)](https://wordpress.org/news/2008/12/coltrane/), **2008**an, azken urteetan ezagutzen dugun administrazio-interfazearen antzekoa diseinatzen dute.

![](img/04.png)

[3.0](https://wordpress.org/news/2010/06/thelonious/) “Thelonious” bertsioak aldaketa handiak ekarri zituen **2010**ean: **post motak, taxonomiak, funts eta goiburu pertsonalizatuak eta nabigazio-menuak, besteak beste.**

Azkenik, 2018an aldaketa garrantzitsu bat gertatu da [5.0](https://wordpress.org/news/2018/12/bebo/) bertsioarekin ([“Bebo Valdés”](https://es.wikipedia.org/wiki/Bebo_Vald%C3%A9s)), [Gutenberg](https://es.wordpress.org/plugins/gutenberg/) izeneko blokeen ikusizko editore berriarekin.

![](img/10.jpg)

## Zenbait ezaugarri

* PHP eta MySQL (MariaDB gaur egun) datu basean oinarrituta.
* GPL Lizentzia librea.
* [API REST](https://developer.wordpress.org/rest-api/) (“REpresentational State Transfer”) [JSON](https://eu.wikipedia.org/wiki/JavaScript_Object_Notation)-ekin (JavaScript Object Notation).
* Argitaratzen diren elementu nagusiak hauek dira: postak edo sarrerak, dataren eta kategorizatuen arabera ordenatuak, eta orrialde estatikoak.
* Zure irakurleekiko elkarreragina, iruzkinak erabiliz.
* Posta elektroniko bidez albisteetan harpidetzea.
* **Ikusizko txantiloien sistema**, edukiarekiko independentea, pertsonalizatzeko aukerekin.
* [**Widget**](https://wordpress.org/support/article/wordpress-widgets/)-ak berariazko funtzioak dituzten blokeak dira.
* Multiblog.
* Egile edo erabiltzaile ugari, eta haien rolak edo profilak, baimen-maila desberdinak ezartzen dituztenak.
* WYSIWYG editorea.
* **Permalink**-ak (lotura iraunkorrak eta gogoratzeko errazak).
* RSS banaketa (Really Simple Syndication), ATOM, etab. Nik Feedly erabiltzen dut _feed_ irakurle gisa.
* Datu erantsiak eta multimedia-fitxategiak igotzea eta kudeatzea.
* Pluginen edo osagarrien sistema.
* SEO-rako prestatua  (_Search Engine Optimization_).
* Estatistiken orria.

## WordPress-en estatistikak eta erabilera-datuak

W3Techs-en arabera (World Wide Web Technology Surveys), [web gune guztien %38k WordPressekin funtzionatzen dute](https://w3techs.com/technologies/details/cm-wordpress), CMSren merkatuan haien kokapena are menderatzaileagoa da, ez du Joomla edo Drupal bezalako tresnekiko beldurrik.

![](img/05.png)

BuiltWith laginean [CMS teknologiak 1 milioi lekutan banatuta](https://trends.builtwith.com/cms) daude:

![](img/06.png)

Munduan gutxienez [27,021,750 web gune daude WPrekin](https://trends.builtwith.com/cms/WordPress) funtzionatzen:

![](img/07.png)

## Kanpoko loturak

* [WordPress Versions](https://codex.wordpress.org/WordPress_Versions).
* [The Blogging Software Dilemma](https://ma.tt/2003/01/the-blogging-software-dilemma/) By Matt, January 24, 2003.
* Kinsta [WordPress market share](https://kinsta.com/wordpress-market-share/).
* [40+ Most Notable Big Name Brands that are Using WordPress](https://www.wpbeginner.com/showcase/40-most-notable-big-name-brands-that-are-using-wordpress/).
* BuiltWith [CMS Usage Distribution in the Top 1 Million Sites](https://trends.builtwith.com/cms).
* w3techs [Usage statistics and market share of WordPress](https://w3techs.com/technologies/details/cm-wordpress).
* [Matt Mullenweg: State of the Word 2019](https://www.youtube.com/watch?v=LezbkeV059Q&feature=youtu.be).
* creativeminds [The Ultimate Guide to WordPress Statistics (2020)](https://www.cminds.com/ultimate-guide-wordpress-statistics%E2%80%A8%E2%80%A8/).
* es.wordpress.org plugins [Gutenberg](https://es.wordpress.org/plugins/gutenberg/).
* [From Kubrick to Twenty Sixteen: A History of WordPress Default Themes](https://www.elegantthemes.com/blog/editorial/from-kubrick-to-twenty-sixteen-a-history-of-wordpress-default-themes).
* wordpress.org [Announcing WordPress 1.5](https://wordpress.org/news/2005/02/strayhorn/) February 17, 2005 by Matt Mullenweg.
* wpbeginner [The History of WordPress from 2003 – 2019 (with Screenshots)](https://www.wpbeginner.com/news/the-history-of-wordpress/).
* wpbeginner [Evolution of WordPress User Interface (2003 – 2019)](https://www.wpbeginner.com/showcase/evolution-of-wordpress-user-interface-2003-2009/).
* The New York Times [Invasion of the ‘Blog’: A Parallel Web of Personal Journals](https://www.nytimes.com/2000/12/28/technology/invasion-of-the-blog-a-parallel-web-of-personal-journals.html) By David F. Gallagher (2000).
* [El Enemigo Conoce el Sistema](https://www.casadellibro.com/libro-el-enemigo-conoce-el-sistema/9788417636395/9501752) de Marta Peirano. Capítulo 5 - La promesa de la blogosfera: vivir para contarlo juntos.
* Julen Orbegozo: [“Redes sociales, internet (y política)”](https://kulturetxea.github.io/charla-soberania-tecnologica/presentacion_julen_orbegozo.html).







