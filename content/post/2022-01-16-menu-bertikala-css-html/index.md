---
title: Nabigazio menu bertikal bat sortu HTML eta CSS erabilita
subtitle: HTML5 eta CSS3
date: 2022-01-16
draft: false
description: HTML5 eta CSS3
tags: ["HTML","CSS","Menuak"]
---

<!-- vscode-markdown-toc -->
* 1. [Adibideak](#Adibideak)
	* 1.1. [Oinarrizko menua](#Oinarrizkomenua)
		* 1.1.1. [Aktibo/uneko nabigazio-lotura](#Aktibounekonabigazio-lotura)
		* 1.1.2. [Estekak erdian lerrokaty eta ertzak gehitu](#Estekakerdianlerrokatyetaertzakgehitu)
		* 1.1.3. [Altuera osoa duen menua](#Altueraosoaduenmenua)
* 2. [Sidenaveko goitibeherako menua](#Sidenavekogoitibeherakomenua)
* 3. [Beste adibide batzuk](#Besteadibidebatzuk)
* 4. [Barne loturak](#Barneloturak)
* 5. [Baliabideak](#Baliabideak)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Nabigazio-barra bertikal bat eraikitzeko, `<li>` zerrendako elementuak erabili ditzakezu `<a>` elementuak barruan dutela (esteka etiketak barruan sartu eta estiloa eman CSS erabilita).

[src/01.html](src/01.html)

![](img/01.png)

```css
li a {
  display: block;
  width: 100px;
}
```

Azalpen txiki batzuk:

* `display: block;`: Lotuneak bloke-elementu gisa ikustean, lotura-eremu guztiak klik egiten du (ez testua bakarrik), eta zabalera (eta _padding_, _margin_, _height_, etab.) zehazteko aukera ematen digu. nahi baduzu) (W3Schools [CSS display Property](https://www.w3schools.com/cssref/pr_class_display.asp)).
* `width: 60px;`: Bloke-elementuek lehenetsitako zabalera osoa betetzen dute. 100 pixeleko zabalera zehaztu nahi dugu.

Halaber, `<ul>` elementuaren zabalera ezar dezakezu (eta `<a>`-rena ezabatu), bloke-elementu gisa agertzen direnean zabalera osoa hartuko baitute. Horrek aurreko adibidearen emaitza bera ekarriko du:

[src/02.html](src/02.html)

##  1. <a name='Adibideak'></a>Adibideak

###  1.1. <a name='Oinarrizkomenua'></a>Oinarrizko menua

Sortu oinarrizko nabigazio-barra bertikala hondo griseko kolorearekin, eta aldatu loturen hondoko kolorea, erabiltzaileak sagua haien gainean mugitzen duenean:

![](img/02.gif)

[src/03.html](src/03.html)

```css
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            width: 200px;
            background-color: #f1f1f1;
        }

        li a {
            display: block;
            color: #000;
            padding: 8px 16px;
            text-decoration: none;
        }
        
        li a:hover {
            background-color: #555;
            color: white;
        }
```

####  1.1.1. <a name='Aktibounekonabigazio-lotura'></a>Aktibo/uneko nabigazio-lotura

Adibide honetan, klase "aktibo" bat sortzen dugu, hondo berdeko kolore batekin eta testu zuri batekin. Klasea "Home" estekan gehitzen da.

![](img/03.gif)

[src/04.html](src/04.html)

```css
        li a.active {
            background-color: #04AA6D;
            color: white;
        }

        li a:hover:not(.active) {
            background-color: #555;
            color: white;
        }
```

####  1.1.2. <a name='Estekakerdianlerrokatyetaertzakgehitu'></a>Estekak erdian lerrokaty eta ertzak gehitu

Adibide honetan, nabigazio-estekak zentratu eta nabigazio-barrari ertza gehitzen diogu.

Gehitu `text-align:center` `<li>` edo `<a>` elementuei estekak zentratzeko.

Gehitu `border: 1px solid #555;` `<li>` eta `<ul>` elementuei ertza gehitzeko.

![](img/04.gif)

[src/05.html](src/05.html)

####  1.1.3. <a name='Altueraosoaduenmenua'></a>Altuera osoa duen menua

`position: fixed;` itsasita gelditzen da bere posizioan, _scroll_ egin arren.

![](img/05.gif)

[src/06.html](src/06.html)

##  2. <a name='Sidenavekogoitibeherakomenua'></a>Sidenaveko goitibeherako menua

* [src/07.html](src/07.html)
* [src/08.html](src/08.html)

##  3. <a name='Besteadibidebatzuk'></a>Beste adibide batzuk

* CodePen [CSS Menu bertikala beti begibistan](https://codepen.io/fptxurdinaga/pen/KKyVVPd).

##  4. <a name='Barneloturak'></a>Barne loturak

* [Oinarrizko nabigazio menu bat sortu HTML eta CSS erabilita](https://iker.l.gitlab.io/eAW_2SM2D_blog/post/2022-01-10-html-css-nabigazio-menua/).

##  5. <a name='Baliabideak'></a>Baliabideak



