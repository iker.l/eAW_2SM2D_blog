---
title: CSS Grid (02)
subtitle: HTML5 eta CSS3
date: 2022-01-14
draft: false
description: HTML5 eta CSS3
tags: ["HTML","CSS","CSS - Grid","Grid"]
---

[`grid-template-areas`](https://developer.mozilla.org/es/docs/Web/CSS/grid-template-areas) propietateak izenen bidez identifikatzen ditu grid azalerak ([grid areas](https://developer.mozilla.org/es/docs/Glossary/Grid_Areas)).

[src/01.html](src/01.html)

![](img/01.png)

[`grid-template-rows`](https://developer.mozilla.org/es/docs/Web/CSS/grid-template-rows) eta [`grid-template-columns`](https://developer.mozilla.org/es/docs/Web/CSS/grid-template-columns) erabili dezakegu honekin batera.

```css
.grid {  
  display: grid;
  grid-template-areas:
    "bat    bat     bi      bi"
    "bat    bat     bi      bi"
    "hiru   hiru    lau     lau"
    "hiru   hiru    lau     lau";  
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: repeat(4, 50px);  
}
```

[src/02.html](src/02.html)

Tamaina finkoko lauki-sarea sor dezakezu, pixelak erabiliz, adibidez. Lauki-sare bat ere sor daiteke, ehunekoekin malguak diren tamainak erabiliz edo xede horretarako diseinatutako **`fr` (frakzioa) neurri-unitate berriarekin**.

[`repeat`](https://developer.mozilla.org/es/docs/Web/CSS/repeat()) errepikatzen diren elementuak modu laburtuan adierazteko modua da.  

Beste adibide bat: [src/03.html](src/03.html).

![](img/02.png)


## Kanpo estekak

* [https://developer.mozilla.org/es/docs/Web/CSS/grid-template-areas](https://developer.mozilla.org/es/docs/Web/CSS/grid-template-areas)
* [https://developer.mozilla.org/es/docs/Glossary/Grid_Areas](https://developer.mozilla.org/es/docs/Glossary/Grid_Areas).
* [https://www.smashingmagazine.com/understanding-css-grid-template-areas/](https://www.smashingmagazine.com/understanding-css-grid-template-areas/).

