---
title: Orrialdeak
subtitle: WordPress hastapenak
date: 2021-11-08
draft: false
description: WordPress hastapenak
tags: ["WordPress","WP","WordPress - Hastapena","WordPress - Orrialdeak"]
---

<!-- vscode-markdown-toc -->
* 1. [Orrialdeak](#Orrialdeak)
* 2. [Orrialde berri bat sortu](#Orrialdeberribatsortu)
	* 2.1. [Orrialde gurasoa](#Orrialdegurasoa)
	* 2.2. [Ordenatu](#Ordenatu)
* 3. [Orriak kudeatu](#Orriakkudeatu)
* 4. [Hasierako orria sortzea](#Hasierakoorriasortzea)
	* 4.1. [Hasierako orri pertsonalizatua gaitu](#Hasierakoorripertsonalizatuagaitu)
	* 4.2. [Hasierako orria pertsonalizatu](#Hasierakoorriapertsonalizatu)
* 5. [Baliabideak](#Baliabideak)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Orrialdeak'></a>Orrialdeak

Hasiera batean, WordPress blog soil bat bezala jaio zen eta bidalketak bakarrik sor zitezkeen, `[2005eko otsailetik](https://codex.wordpress.org/WordPress_Versions) aurrera ordea, orriak WordPress-en sartu ziren. Izan ere, orriak [Custom Post Type](https://codex.wordpress.org/Post_Types) bat dira. Hau da, orriak post mota berezia dira.

Lehen begiratuan, orrialdeak bidalketen oso antzekoak dira. Biek dituzte izenburu eta edukiak sartzeko eremua (testua, argazkia, multimedia, etab). Ala ere, orrialdeak bidalketetatik desberdin erabiltzen dira. Lehenik, **orriek ez dute kategoriarik edo etiketarik**  (orriak ez dira kategorizatu behar, web gune gehienetan askoz orrialde gutxiago baitaude bidalketak baino). Gainera, post-ak zure blogarenak dira, etengabe hedatzen den argitalpen edo bidalketa berriekin. **Bidalketak erregularki gehitzen dira, orriak estatikoagoak dira eta normalean ez da hainbeste aldatzea espero**.

Laburbilduz, **gomendatzen dizuet orrialdeak eduki estatikoko pieza gisa e irudikatzea eta bidalketak aldizkako argitalpen diren artikulu gisa pentsatzea**.

Edozein webgunek izan ditzazkeen **orrialdeen adibideak**:

- Azaleko orria (_home_).
- Kontaktu orria (non gaude, harremanetarako formularioa, ...).
- Nor gara.
- Bilatzaile berezitua.
- Baldintza legalak (3 orri): Pribatasun politika, Cookien politika eta Lege Oharra.

##  2. <a name='Orrialdeberribatsortu'></a>Orrialde berri bat sortu

Orrialde berri bat sortzeko jo **"Orrialdeak | Berria gehitu"** menuko aukerara. Editorea bidalketen berdintsua da, blokeetan oinarrituta. 

Orri berri bat sortzeko behar duzun gutxieneko elementuak bi dira: izenburua eta edukiren bat. Ondoren egin klik "Argitaratu" botoiaren gainean, post bat balitz bezala. Zure orri berriak URL helbide bakarra izango du eskuragarri. 

![](img/01.png)

Adibidea: [https://fptxurdinaga.server1.trinchera.dev/lege-oharra/](https://fptxurdinaga.server1.trinchera.dev/lege-oharra/).

**BURUAN DUZUN WEBGUNEAREN ARABERA, OHIKOA DA ORRI BAT DISEINATZEA ETA ONDOREN _THEME_ EDO ITXURA ATALEAN AZALEKO HORRI (_HOME_) BEZALA EZARRI.**

Bloketan oinarritutako editorearen albo-barraren konfigurazioan atal gehienak ezagutuko dituzu. Gauza bera egiten dute orrialdeetan eta bidalketetan.

Hitz egin dezagun atal berri bakarraz, **Orriaren atributuak** izenaz, orrialde gurasoa eta ordena bezalako elementuak dituena:

![](img/02.png)

###  2.1. <a name='Orrialdegurasoa'></a>Orrialde gurasoa

WordPress-ek bere **orriak hierarkikoki egituratzeko** aukera ematen du. Beraz, orri eta azpi-orri gehiagotan antola ditzakezu, hau erabilgarria da zure Webguneak orrialde asko izango baditu. Adibidez, blog baten lagun talde bat idazten bagaude autore moduan, bakoitzak orri bat izango luke berari buruz (_About_ edo Nor Naiz), horiek orrialde nagusiko azpiorrialdeak izango lirateke. Orrialde hauetako bat gehitzen ariko banintz, lehenbizi _About_ orri berri bat sortuko nuke (nagusia), gero beste orrialde bat sortuko nuke niretzat bakarrik, _About Iker_ izena duena, eta azkenik _About_ orrialde nagusia aukeratuko nuke orrialde berriaren guraso bezala.

###  2.2. <a name='Ordenatu'></a>Ordenatu

Besterik adierazi ezean, sortzen diren orri guztiak alfabeto-ordenaren arabera aurkeztuko dira. Beste ordenaren batean nahi badituzu, hori zehatz dezakezu eskaera- zure orrialdeetarako zenbakiak sartuta. Zintzoa izateko, **hau ez da oso metodo argia orriak berrantolatzeko, bereziki menu batean agertzea nahi baduzu**. Askoz errazago egin dezakezu, menuekin zuzenean lan eginez

##  3. <a name='Orriakkudeatu'></a>Orriak kudeatu

Zure web guneko orri guztien zerrenda ikusteko, jo menu nagusiko "Orrialdeak" atalera. Pantaila-argazki honetan agertzen denaren antzeko zerbait ikusiko duzu:

![](img/03.png)

Dagoeneko, zerrenda-formatu horrek familiarra ematen hasi beharko luke zuretzako. Zure orrialdeen zerrenda duzu, eta lerro bakoitzean esteka erabilgarri batzuk daude, **editatu, azkar editatu, zaborra bota edo orria ikusteko**. Autore baten izenean klik egin dezakezu zerrenda autore horren arabera iragazteko. Goiko bi estekak, Guztiak eta Argitaratuak, erabil ditzakezu orriak egoeraren arabera iragazteko. 

![](img/04.png)

Dataren arabera ere iragazten da, goitik beherako menuaren bidez, orrialdeen zerrendaren gainean.

Gainera, masiboki editatu ditzakezu orriak edo zakarrontzira bidali "Multzotako ekintzak" menua erabiliz zerrendaren goiko eta beheko aldean. Azkenik, goiko aldean bilaketa-koadroarekin bila dezakezu zure orrietan.

##  4. <a name='Hasierakoorriasortzea'></a>Hasierako orria sortzea

Web gunean sor ditzakezun beste orri guztien artean oso berezia den orrialde bat dago. Hori da **orri nagusia, azaleko orria edo hasierako orria** ere deitzen zaio. Modu errazean ulertzerko, web guneko helbide nagusira joaten direnean, orri nagusia bisitatuko dute.

**AZALEKO HORRIA NOLA EZARRI _THEME_-aren ARABERAKOA DA**.

Hau azaltzeko WordPress-ek defektuz gaituta dakarren [Twenty Nineteen](https://es.wordpress.org/themes/twentynineteen/) _theme_-a erabiliko dugu.

![](img/05.png)

Has gaitezen bi orrialde berri sortuz: Azala eta Blog.

###  4.1. <a name='Hasierakoorripertsonalizatuagaitu'></a>Hasierako orri pertsonalizatua gaitu

WordPress-i jarraibideak eman behar dizkiogu orrialde nagusiaren edukia zehaztean bi orrialde horiek erabiltzen hasteko.

Joan menu nagusian "Ezarpenak | Irakurketa" atalera.

![](img/06.png)

**Gorde aldaketak**.

###  4.2. <a name='Hasierakoorriapertsonalizatu'></a>Hasierako orria pertsonalizatu

Azaleko orria editatu, izenburu adierazgarri bat jarri. Orain ausnartu sortu behar duzun webgunearen arabera zer erakutsi nahi duzun azaleko orrian. Adibidez ongi etorri moduko testu bat gehitu. Horren ondoren, oraindik landu ez dugun bloke berri bat gehituko dugu. Blokeak **Azken bidalketak** izena du 

![](img/07.png)

Bloke hau gehitu bezain laster, zure azken bidalketekin beteko da. Aldatu diseinua, zerrenda bezala ikusi beharrean saretxo moduan ikusi nahi dugu (_grid_):

![](img/08.png)

Ondoren blokearen ezarpenak aldatuko ditugu alboko barran.

![](img/09.png)

![](img/10.png)

![](img/11.png)



##  5. <a name='Baliabideak'></a>Baliabideak

* [https://wordpress.org/support/article/pages/](https://wordpress.org/support/article/pages/).















