---
title: Zebitzariko fitxategiak Kudeatu FTP bidez
subtitle: XAMPP eta FileZilla
date: 2021-10-29
draft: false
description: XAMPP eta FileZilla
tags: ["WordPress","WP","Instalazioa","Windows","XAMPP"]
---

XAMPP-ek [FileZilla](https://filezilla-project.org/) zerbitzariaren bertsioa du, kode irekiko FTP soluzio bat (_open source_). Hori dela eta, erraza da fitxategiak XAMPP ingurunetik eta ingurunera transferitzea, [FTP](https://eu.wikipedia.org/wiki/Fitxategien_Transferentziarako_Protokoloa) erabiliz.

## FileZilla konfigurazioa

FTP erabiliz fitxategiak transferitzeko, lehenbizi FileZillako FTP zerbitzaria konfiguratu behar duzu. Jarraitu hurrengo urratsei.

Lehendabizi ziurtatu FileZilla funtzionatzen ari dela XAMPP aginte panelean.

![](img/01.PNG)

Hasi FileZilla administratzeko interfazea XAMPP kontrol-paneleko "Admin" botoian klik eginez.  "Connect to Server" elkarrizketa-koadroan, utzi balio guztiak dauden bezala, eta egin klik "OK" botoian.

![](img/02.PNG)

Orain FileZilla zerbitzariaren kontsolara iritsi beharko zinateke.

![](img/03.PNG)

Orain, gutxienez FTP erabiltzaile bat konfiguratu behar duzu. Horretarako, egin klik "Edit > Users" menuan. Horrek erabiltzailea kudeatzeko interfazera eramango zaitu

![](img/04.PNG)

Egin klik "Add" botoian, erabiltzaile-kontu berri bat gehitzeko. Sartu izen bat erabiltzaile-kontu berrirako (nik "wp-ftp" aukeratu dut erabiltzaile izen bezala).

![](img/05.PNG)

Hautatu eta markatu "Password" eremua eta sartu pasahitz bat erabiltzaile berriarentzat ("fp$txurd1" adibidez).

![](img/06.PNG)

Ondoren, erabiltzaileak zein karpetatara sartzeko aukera duen definitu behar duzu. Horretarako, egin klik ezkerreko nabigazio-menuko "Shared folders" azpielementuan.

Egin klik "Add" botoian, eta hautatu XAMPP instalazio-direktorioko htdocs (c:\xampp\htdocs) azpidirektorioa erabiltzailearen hasierako direktorio gisa, eman erabiltzaileari eskura dituen eskubide guztiak direktorio honetan.

![](img/07.PNG)

Itxi FileZilla Server aplikazioa (File > Quit).

## Frogatu 

MS-DOS Komando-lerrotik probatu konexioa.

![](img/08.PNG)

## FileZilla client

Komando-lerrotik fitxategiak transferi ditzakegu, baina interfaze grafikoa duen bezero bat erosoagoa da. "FileZilla Client" bezeroa erabiliko dugu ([https://filezilla-project.org/](https://filezilla-project.org/)).

![](img/09.PNG)

Descargatu, instalatu eta exekutatu lehen aldiz, sartu konexio-xehetasunak ondoren

* Ostalaria: "locahost" edo "127.0.0.1" (sareko beste ordenagailu batetik konektatzeko IP helbidea ezagutu behar duzu).
* Erabiltzaile izena eta Pasahitza: Gorago sortu duzun erabiltzailea FileZilla zerbitzarian.

"Konexio azkarra" botoian klik egin.

![](img/10.PNG)

Zure FTP bezeroak orain zerbitzarira konektatu behar du eta "C:\xampp\htdocs" direktorioan sartu, hau da, web-zerbitzariaren dokumentu lehenetsiaren erroan.





