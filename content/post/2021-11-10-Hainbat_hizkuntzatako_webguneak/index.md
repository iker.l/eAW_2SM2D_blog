---
title: Hainbat hizkuntzatako webguneak
subtitle: Polylang plugin-a
date: 2021-11-10
draft: false
description: Polylang plugin-a
tags: ["WordPress","WP","WordPress - Hastapena","WordPress - Pluginak","Polylang"]
---

<!-- vscode-markdown-toc -->
* 1. [Polylang WordPress instalatzea](#PolylangWordPressinstalatzea)
* 2. [Menuak eta ezarpen orokorrak](#Menuaketaezarpenorokorrak)
	* 2.1. [Hizkuntzak](#Hizkuntzak)
* 3. [Sortu edukia hizkuntza nagusian, Polylang erabiliz](#SortuedukiahizkuntzanagusianPolylangerabiliz)
* 4. [Sortu menua banderekin Polylang plugin-ean](#SortumenuabanderekinPolylangplugin-ean)
* 5. [Baliabiadeak](#Baliabiadeak)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Polylang plugin-ari esker zure web guneak hainbat hizkuntzatan eduki ditzake edukiak.

Polylangen faktore bereizgarria, lan bera egiten duten beste plugin batzuen aldean (adibidez, [WPML](https://wpml.org/)), Polylangen [doako bertsioa eta ordainezkoa dauka (Polylang Pro )](https://polylang.pro/products/), eta, horren bidez, funtzio gehienak egin ditzakegu.

![](img/01.png)

Hurrengo taulan bi bertsioen arteko desberdintasunak ikus ditzakezu.

![](img/02.png)

##  1. <a name='PolylangWordPressinstalatzea'></a>Polylang WordPress instalatzea

Lehenik eta behin, Polylang instalatu behar duzu zure web-orrian. Horretarako, biltegi ofizialetik edo WordPress-en plugin-bilaketatik, Polylang arazorik gabe aurkitu eta instalatu ahal izango duzu.

![](img/03.png)

Laguntzaile honek Polylangen doikuntzak konfiguratzen lagunduko dizu, eta berehala hasiko zara zure web gune eleaniztunarekin.

Lehenik, zure web gunean erabiliko dituzun hizkuntzak definituko ditugu.

![](img/04.png)

![](img/05.png)

![](img/06.png)

![](img/07.png)


##  2. <a name='Menuaketaezarpenorokorrak'></a>Menuak eta ezarpen orokorrak

![](img/08.png)

Instalatuta duzunean, kontrol-panelean ezkerreko nabigazio-barran Hizkuntzak izeneko atal berri bat agertuko zaizu. Atal horrek hau du:

* **Languages**: Hemen, zure web orrirako nahi duzun hizkuntzak sor ditzakezu (Polylangen tutorial honen hurrengo urratsean azalduko dizut).
* **Strings translations**: Atal honetan, WordPress-en zure plantillako kate erlatibo batzuk eskuz itzuli ahal izango dituzu. Adibidez, «gehiago irakurri» tipikoa, orriaren izenburua edo deskribapena.
* **Settings**: Polylang eskaintzen dizkizun aparteko doikuntza batzuk ikusiko dituzu hemen. Dena lehenespen gisa utz dezakezu, baina beti egin dezakezu begirada bat, aukeraren bat komeni zaizun edo ez ikusteko. **Nire aholkua atal honetako ezer ez ukitzea da**.
* **Lingotek**: Polylang testuen itzulpen automatikoko [Lingotek](https://lingotek.com/) tresnarekin bateragarria da. Horrek lan asko aurreztuko dizu, baina itzulpen automatiko guztiek bezala, emaitzak ez dira nahi bezain onak izaten. Beraz, **nire aholkua da funtzio hori ahal den neurrian ez erabiltzea**.

###  2.1. <a name='Hizkuntzak'></a>Hizkuntzak

![](img/09.png)

Atal honetan hizkuntzak gehitu edo ken ditzakegu, lehenetsitako hizkuntza nagusia izar batekin markatuta dago, zerrendan bertan alda daiteke, hizkuntza berriak gehi ditzakegu edo daudenak edita ditzakegu.


##  3. <a name='SortuedukiahizkuntzanagusianPolylangerabiliz'></a>Sortu edukia hizkuntza nagusian, Polylang erabiliz

Plugina instalatu eta hizkuntzak sortu ondoren, edukia gehitu beharko duzu haientzat. Oraingoz ez baduzu ezer, bidalketa edo orri berri bat sortu dezakezu, guk sarrera berri bat sortuko dugu hasteko.

Bidalketa sortzen ari zarenean, erreparatu doikuntzak ikusten diren eskuineko barrari, atal berri bat dago, erabiliko duzun hizkuntza adierazten dizun hizkuntza-hautagailua duena.

![](img/10.png)

Aldiz, zure web edo blogean edukia sortuta bazenuen, baliteke WordPress plugin polylang instalatzeko hizkuntza nagusia esleitu ez izana. **Sortutako orri edo sarrera bakoitzean aldatu beharko duzu**.

Horrela, dena behar bezala egin baduzu, zure orrialde edo bidalketen zerrendan zutabe berri bat agertuko zaizu, hizkuntza bakoitzeko banderatxoak eta orria itzulpena duen ala ez adierazten duen kontrol-laukia.

![](img/11.png)


##  4. <a name='SortumenuabanderekinPolylangplugin-ean'></a>Sortu menua banderekin Polylang plugin-ean

Polylangen tutorial honetako hurrengo urratsa da zure menu nagusian hizkuntza hautatzeko banderatxo famatuak gehitzea.

Gehitzeko, hona jo behar duzu: **Itxura > Menuak**.  

Hemen ikusiko duzu "Hizkuntza-konmutadorea" edo "Languaje switcher" izeneko aukera bat agertzen dela: hautatu eta gehitu zure menura. Hori egitean, antzeko zerbait izango duzu:

![](img/12.png)

Horrela ikusten da:

![](img/13.png)

Hurrengo urratsa menua beste hizkuntzan sortzea da, eta hautagailu hori ere sartzea.

![](img/14.png)

![](img/15.png)



##  5. <a name='Baliabiadeak'></a>Baliabiadeak

* [https://polylang.pro/](https://polylang.pro/).
* [https://wpml.org/purchase/](https://wpml.org/purchase/).