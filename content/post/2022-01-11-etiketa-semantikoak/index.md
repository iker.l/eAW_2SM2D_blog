---
title: Etiketa semantikoak
subtitle: HTML5 eta CSS3
date: 2022-01-11
draft: false
description: HTML5 eta CSS3
tags: ["HTML","CSS","Etiketak"]
---

<!-- vscode-markdown-toc -->
* 1. [Etiketa semantikoak](#Etiketasemantikoak)
	* 1.1. [Section etiketa](#Sectionetiketa)
	* 1.2. [Article etiketa](#Articleetiketa)
	* 1.3. [Header etiketa](#Headeretiketa)
* 2. [Adibideak](#Adibideak)
* 3. [Kanpo estekak](#Kanpoestekak)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

[HTML5](https://eu.wikipedia.org/wiki/HTML5) bertsioaren etorrerakin etiketa berriak gehitu dira, eta erabilgarritasunaren edo ezaugarrien arabera sailka ditzakegu. Sailkapen horietako bat **etiketa semantikoak dira**, hemen azalduko ditugunak.

![](img/01.png)

[[Euskaltzaindia - Semantika]](https://www.euskaltzaindia.eus/index.php?sarrera=semantika&option=com_hiztegianbilatu&view=frontpage&Itemid=410&lang=eu&bila=bai).

Hori da Euskaltzaindiaren definizioa, eta, beraz, erraz ulertzen da HTML hizkuntzako etiketa mota hori: **dokumentuaren zatiei esanahia ematen dietenak**.

Beste era batera esatearren, **etiketak barnean zer edukin duten adierazten dute**, HTML dokumentua web-bezero batean erakustean formateatu beharrean.

##  1. <a name='Etiketasemantikoak'></a>Etiketa semantikoak

| Etiketa | Esanahia | 
| -------------- | --------- | 
| [`<header>`](https://developer.mozilla.org/es/docs/Web/HTML/Element/header)      | HTML elementuak sarrera-edukia adierazten du, normalean sarrera- edo nabigazio-laguntzen talde bat. Goiburuko elementu batzuk izan ditzake, baina baita logotipo bat, bilaketa-formulario bat, egile-izen bat eta beste elementu batzuk ere.   
| [`<nav>`](https://developer.mozilla.org/es/docs/Web/HTML/Element/nav) | orrialde baten atal bat da, eta haren helburua da nabigazio-estekak ematea, orrialde berean edo beste dokumentu batzuetan. Nabigazio-atalen ohiko adibideak menuak, eduki-taulak eta aurkibidea dira. |
| [`<aside>`](https://developer.mozilla.org/es/docs/Web/HTML/Element/aside) | dokumentuaren eduki nagusiarekin zeharka lotuta dagoen edukia duen orrialde baten atala irudikatzen du. Atal horiek, askotan, alboko barra gisa edo txertaketa gisa irudikatzen dira, eta alboan azalpen bat dute, hala nola glosarioaren definizioa, zeharka lotutako elementuak, hala nola publizitatea, egilearen biografia edo web-aplikazioetan, profilaren informazioa edo lotutako blogetarako estekak. |
| [`<main>`](https://developer.mozilla.org/es/docs/Web/HTML/Element/main) | dokumentu edo aplikazio baten `<body>`-aren eduki nagusia adierazten du. |
| [`<article>`](https://developer.mozilla.org/es/docs/Web/HTML/Element/article) | Eduki-atal bat da, eta modu independentean bana daiteke. Foro bateko post bat, egunkari-artikulu bat, blog bateko sarrera bat, iruzkin bat eta abar izan daitezke. |
| [`<section>`](https://developer.mozilla.org/es/docs/Web/HTML/Element/section) | Orrialde bateko atal generiko bat irudikatzen du. Gaien arteko erlazioa duten edukiak biltzen ditu. |
| [`<footer>`](https://developer.mozilla.org/es/docs/Web/HTML/Element/footer) | Orri-oin bat irudikatzen du hura duen elementuarentzat. Normalean, nork idatzi zuen, lotutako dokumentuetarako estekak, egile-eskubideen datuak edo antzekoak biltzen ditu. |

![img/03.png](img/03.png)

###  1.1. <a name='Sectionetiketa'></a>Section etiketa

[`<footer>`](https://developer.mozilla.org/es/docs/Web/HTML/Element/footer) elementuak atal bat definitzen du dokumentu batean.

[W3C-ren HTML dokumentazioaren](https://www.w3.org/TR/2011/WD-html5-20110525/sections.html) arabera: "Atal bat eduki-multzo tematiko bat da, normalean izenburu batekin".

Elementu bat non erabil daitekeen jakiteko adibideak:

* Kapituluak
* Sarrera
* Albisteak
* Harremanetarako informazioa

[src/sekzioa-01.html](src/sekzioa-01.html).

###  1.2. <a name='Articleetiketa'></a>Article etiketa

[`<article>`](https://developer.mozilla.org/es/docs/Web/HTML/Element/article) elementuak eduki independentea eta autonomoa zehazten du.

Artikulu batek berez izan behar du zentzua, eta web guneko gainerako ataletatik bereizita banatu behar da.

Elementu hau erabil daitekeen adibideak:

* Foro bateko _post_-ak
* Blog-sarrerak
* Erabiltzailearen iruzkinak
* Prentsa-artikuluak

[src/article-01.html](src/article-01.html).

###  1.3. <a name='Headeretiketa'></a>Header etiketa

[`<header>`](https://developer.mozilla.org/es/docs/Web/HTML/Element/header)  Sarrera-edukirako edo nabigazio-esteken multzorako edukiontzi bat da “heads” elementua.

Normalean, elementu hauek izaten ditu:

* abiapuntuko elementu bat edo gehiago (“h1” - “h6”)
* logotipoa edo ikonoa
* egileen informazioa

**Oharra**: HTML dokumentu batean [`<header>`](https://developer.mozilla.org/es/docs/Web/HTML/Element/header) hainbat elementu izan ditzakezu. Hala ere `<footer>`, `<address>` edota beste `<header>` elementuren baten barruan ezin da jarri.

[src/header-01.html](src/header-01.html)

##  2. <a name='Adibideak'></a>Adibideak

[src/index.html](src/index.html).

![](img/02.png)

##  3. <a name='Kanpoestekak'></a>Kanpo estekak

* [https://www.w3schools.com/html/html5_semantic_elements.asp](https://www.w3schools.com/html/html5_semantic_elements.asp).
* [https://www.arkaitzgarro.com/html5/capitulo-2.html](https://www.arkaitzgarro.com/html5/capitulo-2.html).





