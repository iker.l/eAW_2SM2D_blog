---
title: Lehen pausuak WordPress administrazaile bezala
subtitle: WordPress hastapenak
date: 2021-11-01
draft: false
description: WordPress hastapenak
tags: ["WordPress","WP","WordPress - Hastapena","WordPress - Administrazailea"]
---

<!-- vscode-markdown-toc -->
* 1. [Saioa WordPress-en hasi](#SaioaWordPress-enhasi)
* 2. [Administratzaile panela](#Administratzailepanela)
* 3. [Guneko ezarpenak aldatzen](#Gunekoezarpenakaldatzen)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


##  1. <a name='SaioaWordPress-enhasi'></a>Saioa WordPress-en hasi

WordPress-ekin sortutako edozein tokitako saio-hasiera URL honetan dago: https://domeinua/wp-admin. Gure zerbitzari lokalean bagaude [http://localhost/wordpress/wp-admin](http://localhost/wordpress/wp-admin).

![](img/01.PNG)

Salbuespen batzuk ere badaude; izan ere, segurtasun-arrazoiak direla eta, gomendagarria da aldatzea. 

![](img/02.png)

[WPS Hide Login](https://wordpress.org/plugins/wps-hide-login/) _plugin_-ak saio-hasierako orriaren helbidea aldatzeko aukera ematen du.

[.htaccess](https://es.wikipedia.org/wiki/.htaccess) neurri gehigarri gisa erabiltzea ohikoa da [Apache HTTP zerbitzarietan](https://eu.wikipedia.org/wiki/Apache_HTTP_zerbitzaria).

##  2. <a name='Administratzailepanela'></a>Administratzaile panela

Barruan, idazmahaia edo administratzaile-panela ikusiko dugu, eta goian, ezkerrean, gunearen izena agertuko da beti.

![](img/03.png)

Goiko aldean administratzaile-barra dago. elementu hauek ditu:

![](img/04.png)

* Goitik beherako menu bat, WPren logoarekin, zenbait esteka dituena WordPress-i buruz (WPren instalazioari buruzko xehetasunak), [WordPress.org](https://es.wordpress.org/), [Dokumentazioa](https://codex.wordpress.org/) (codex.wordpress.org), [Euskarria](https://es.wordpress.org/support/) eta [iradokizunak](https://es.wordpress.org/support/forum/comunidad/peticiones-y-feedback/).
* Zure lekuaren alde publikorako esteka.
* Eguneratzeak eta jarduera.
* Bisitarien iruzkinak.
* Gehitu: Sarrera, mediateka, orrialdea, erabiltzailea.
* Azkenik, eskuineko izkinan, gure erabiltzaile-profila agertzen da.

Halaber, “Pantaila-aukerak” fitxan (wp-admin-eko beste leiho askotan agertuko da), klik egiten baduzu eta zabaltzen baduzu, uneko orrian bistaratzeko edo ezkutatzeko elementuen _checklist_-a bistaratzen da. Aukera horiek aldatu egiten dira orrialde bakoitzean (aukera hauek probatzera eta zure gustura jartzera gonbidatzen zaitut).

“Pantaila-aukerak” fitxaren ondoan “Laguntza” fitxa dago.

![](img/05.png)

Ezkerraldean menu bertikal nagusia dago:

![](img/06.png)

Elementu bakoitzaren gainean klik egin, dagokion atalera sartu edo kurtsorea gainetik gelditu, habiaratutako atalak ikusteko.

Goiko menua eta menu nagusia wp-admin-en orri guztietan daude.

##  3. <a name='Gunekoezarpenakaldatzen'></a>Guneko ezarpenak aldatzen

Instalatu ondoren, tokiari buruzko informazio orokorra aldatu beharko duzu (izenburua eta deskribapen labur bat); orain, aukera nagusiei buruz arituko gara (**Doikuntzak | Orokorrak**).

![](img/07.png)

Hizkuntza aldatu, ordu-eremua ere aldatu nahi izango duzu (Madril edo UTC +1) (garrantzitsua da, batez ere, posten kronologiarako edo argitalpena programatzeko, adibidez).

Kontuan hartu behar duzun beste aukera bat da erabiltzaileak harpidetzeko profil lehenetsia onartuko dituzun.

![](img/08.png)

**Oharra**: Gogoratu aldaketak gorde behar dituzula amaitutakoan.

Amaitu baino lehen, doikuntza bat gehiago egin behar da edozein eduki argitaratu aurretik: lotura iraunkorrak (permalink-ak) "Ezarpenak | Esteka iraunkorrak” atalean.

![](img/09.png)






