---
title: Iradokitako plugin-ak (|)
subtitle: WordPress hastapenak
date: 2021-11-09
draft: false
description: WordPress hastapenak
tags: ["WordPress","WP","WordPress - Hastapena","WordPress - Pluginak"]
---

<!-- vscode-markdown-toc -->
* 1. [RGPD Legea](#RGPDLegea)
	* 1.1. [GDPR Cookie Compliance (CCPA ready)](#GDPRCookieComplianceCCPAready)
		* 1.1.1. [Ezarpenak](#Ezarpenak)
* 2. [Segurtasun plugin-ak](#Segurtasunplugin-ak)
	* 2.1. [WPS Hide Login](#WPSHideLogin)
	* 2.2. [Wordfence](#Wordfence)
	* 2.3. [Limit Login Attempts Reloaded](#LimitLoginAttemptsReloaded)
* 3. [SPAM](#SPAM)
	* 3.1. [Akismet](#Akismet)
* 4. [Babeskopiak](#Babeskopiak)
	* 4.1. [UpdraftPlus](#UpdraftPlus)
* 5. [SEO](#SEO)
	* 5.1. [Yoast SEO](#YoastSEO)
* 6. [Errendimendua](#Errendimendua)
	* 6.1. [W3 Total Cache](#W3TotalCache)
* 7. [Beste](#Beste)
	* 7.1. [Health Check & Troubleshooting – Plugin WordPress](#HealthCheckTroubleshootingPluginWordPress)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='RGPDLegea'></a>RGPD Legea

![](img/06.png)

###  1.1. <a name='GDPRCookieComplianceCCPAready'></a>GDPR Cookie Compliance (CCPA ready)

Europar Batasuneko RGPD Cookien legea betetzeko ezinbestekoa.

![](img/01.png)

Plugin-a Wordpress.org atarian: [https://wordpress.org/plugins/gdpr-cookie-compliance/](https://wordpress.org/plugins/gdpr-cookie-compliance/).

####  1.1.1. <a name='Ezarpenak'></a>Ezarpenak

Doakoa izan arren, oso pertsonaliza daiteke, banner, kolore eta bestelakoetarako logotipo propioarekin.

![](img/02.png)

Webgunea bisitatzean nabarmendutako _banner_-aren testua defini dezakegu:

![](img/03.png)

_Banner_-ean azaltzen diran botoiak gaitu eta testua aldatu:

![](img/04.png)

##  2. <a name='Segurtasunplugin-ak'></a>Segurtasun plugin-ak

###  2.1. <a name='WPSHideLogin'></a>WPS Hide Login

[WPS Hide Login](https://es.wordpress.org/plugins/wps-hide-login/) _plugin_-ak WordPress-eko erabiltzaileak sartzeko URL-a (https://zure_webgunea/**wp-admin**) aldatzen du.

_Plugin_ hau oso gomendagarria da, Interneten gune asko WordPress-en sortu baitira, guztiek URL bera erabiltzen baitute. Horregatik, erasotzaileek eraso automatizatuak egin ditzakete erabiltzaile-kontua asmatzeko, adibidez, _brute force_ deritzona erabilita.

![](img/05.png)

###  2.2. <a name='Wordfence'></a>Wordfence

[Wordfence](https://es.wordpress.org/plugins/wordfence/) WordPress-en _plugin_-a web suebaki bat (_waf - Web application firewall_) da. Etengabe zure guneak dituen mehatxu gehienak geldiarazten ditu, hala nola eskaera maltzurrak, edo nahi gabe irekita egon daitezkeen edo berriki eguneratzeak aplikatu ez diren atzeko ateetan kodeak sartzea. Zure webgunea babesteko ezinbestekoak diren beste segurtasun-neurri batzuk ere baditu.

Segurtasun-plugin honek doako bertsio bat du, baina, horrez gain, funtzio aurreratuagoak dituen ordainketa-bertsio bat ere badu.

###  2.3. <a name='LimitLoginAttemptsReloaded'></a>Limit Login Attempts Reloaded

![](img/12.png)

[Limit Login Attemps Reloaded](https://es.wordpress.org/plugins/limit-login-attempts-reloaded/)-ek indar gordineko erasoak geldiarazten ditu, eta zure lekuaren errendimendua optimizatzen du, sarbide arruntaren bidez egin daitezkeen sarbide-saiakeren kopurua mugatzen baitu.

![](img/13.png)

##  3. <a name='SPAM'></a>SPAM

Bidalketa baten komentarioen atalean _spam_-a jasotzea ohikoa da. Jendeak _spam_ modulo iruzkinak bidaltzeko arrazoi nagusia SEO da. Oro har, SEOri dagokionez, adierazten duten esteken kopurua. leku bat sailkapen-faktore ezaguna da, eta mundu osoko webguneen jabeek ahal dutena egiten dute ahalik eta lotura gehien lortzeko.

###  3.1. <a name='Akismet'></a>Akismet

![](img/14.png)

![](img/15.png)

Doako bertsioa erabilpen pertsonalerako hautatu:

![](img/16.png)

![](img/17.png)

##  4. <a name='Babeskopiak'></a>Babeskopiak

###  4.1. <a name='UpdraftPlus'></a>UpdraftPlus 

![](img/08.png)


##  5. <a name='SEO'></a>SEO

###  5.1. <a name='YoastSEO'></a>Yoast SEO

Egia sinplea da zure lekuko SEOn (_Search engine optimization_) lan egitea, ondo egiten bada, izango dela bilaketa-motorretan duen posizioa igotzea (Google, adibidez), eta egunero bisitari gehiago ekarriko dizkizu.

![](img/09.png)

Ezin baduzu denbora asko eman SEO optimizatzeko erabili [Yoast SEO](https://es.wordpress.org/plugins/wordpress-seo/) _plugin_-a.

Pluginak hasierako konfiguraziorako laguntza eskaintzen du pausus pausu.

![](img/10.png)


##  6. <a name='Errendimendua'></a>Errendimendua

###  6.1. <a name='W3TotalCache'></a>W3 Total Cache

[W3 Total Cache (W3TC)](https://es.wordpress.org/plugins/w3-total-cache/) programak SEO eta zure guneko erabiltzaile-esperientzia hobetzen ditu, webaren errendimendua handituz eta karga-denborak murriztuz.

![](img/11.png)

##  7. <a name='Beste'></a>Beste

###  7.1. <a name='HealthCheckTroubleshootingPluginWordPress'></a>Health Check & Troubleshooting – Plugin WordPress

[_Plugin_ honek](https://es.wordpress.org/plugins/health-check/) zenbait egiaztapen egingo ditu zure WordPress-en instalazioan, konfigurazio-akats komunak eta arazo ezagunak detektatzeko,

![](img/07.png)








