---
title: Oinarrizko nabigazio menu bat sortu HTML eta CSS erabilita
subtitle: HTML5 eta CSS3
date: 2022-01-10
draft: false
description: HTML5 eta CSS3
tags: ["HTML","CSS","Menuak"]
---

<!-- vscode-markdown-toc -->
* 1. [CSS oinarrizko nabigazio-barra](#CSSoinarrizkonabigazio-barra)
* 2. [CSS nabigazio-barra bertikala](#CSSnabigazio-barrabertikala)
	* 2.1. [Adibidea](#Adibidea)
		* 2.1.1. [01](#)
* 3. [CSS Nabigazio-barra horizontala](#CSSNabigazio-barrahorizontala)
	* 3.1. [Inline zerrenda elementuak](#Inlinezerrendaelementuak)
	* 3.2. [Float zerrenda elemantuak](#Floatzerrendaelemantuak)
	* 3.3. [Adibideak](#Adibideak)
		* 3.3.1. [Fondo iluna esta esteken kolorea aldatu sagua gainetik pasatzean](#Fondoilunaestaestekenkoloreaaldatusaguagainetikpasatzean)
		* 3.3.2. [Aktibo dagoen orrialdea / esteka markatu](#Aktibodagoenorrialdeaestekamarkatu)
		* 3.3.3. [Elementu bat eskubian lerrokatu](#Elementubateskubianlerrokatu)
		* 3.3.4. [Ertzak eskubian](#Ertzakeskubian)
* 4. [Kanpo estekak](#Kanpoestekak)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='CSSoinarrizkonabigazio-barra'></a>CSS oinarrizko nabigazio-barra 

Nabigazio barra batek HTML estandarra behar du oinarri gisa, CSS erabilita itxura edo diseinua emango diogu.

Lehen adibidea egiteko HTML zerrendak sortzeko etiketak erabiliko ditugu.

Nabigazio-barra bat, funtsean, esteka-zerrenda bat da, eta, beraz, [<ul>](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ul) eta [<li>](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/li) elementuak erabiltzeak zentzua du:

[src/nabigazio-barra-01.html](src/nabigazio-barra-01.html)

```html
<ul>
  <li><a href="#">Home</a></li>
  <li><a href="berriak.html">Berriak</a></li>
  <li><a href="kontaktua.html">Kontaktua</a></li>
  <li><a href="gu.html">Guri Buruz</a></li>
</ul>
```

Horrela ikusten da nabigatzailean:

![](img/01.png)

Orain, zerrendaren ikonoak, _margin_-a eta betegarria (_padding_) zerrendatik kenduko ditugu CSS erabilita

```css
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
}
```

##  2. <a name='CSSnabigazio-barrabertikala'></a>CSS nabigazio-barra bertikala

Nabigazio-barra bertikal bat eraikitzeko, aurreko orrialdeko kodeaz gain, zerrendako <a> elementuak CSS bidez aldatu ditzakezu:

[src/nabigazio-barra-02.html](src/nabigazio-barra-02.html).

```CSS
li a {
  display: block;
  width: 60px;
}
```

* `display: block;`: Elementu bat bloke-elementu gisa erakusten du (<p> gisa). **Lerro berri batean hasten da, eta zabalera osoa hartzen du**.
* `width: 60px;`: Bloke-elementuek lehenetsitako zabalera osoa betetzen dute. 60 pixeleko zabalera zehaztu nahi dugu (frogatu zabalera gabe edo aldaketak egiten, oso estua bada lerroa bitan banatu dezake).

Halaber, <ul>-ren zabalera ezar dezakezu, eta <a>-ren zabalera ezabatu bloke-elementu gisa agertzen direnean zabalera osoa hartuko baitute. Horrek aurreko adibidearen emaitza bera ekarriko du:

[src/nabigazio-barra-03.html](src/nabigazio-barra-03.html).

```CSS
       ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 60px;
      }

      li a {
        display: block;
      }
```

###  2.1. <a name='Adibidea'></a>Adibidea

####  2.1.1. <a name=''></a>01

**Grisa atzean eta loturen kolore aldaketa sagua gainetik mugitzean**:

[src/nabigazio-barra-04.html](src/nabigazio-barra-04.html).

```CSS
ul {
      list-style-type: none;
      margin: 0;
      padding: 0;
      width: 200px;
      background-color: #f1f1f1;
    }

    li a {
      display: block;
      color: #000;
      padding: 8px 16px;
      text-decoration: none;
    }

    /* Change the link color on hover */
    li a:hover {
      background-color: #555;
      color: white;
    }
```

![](img/02.gif)

**Aktibo/uneko nabigazio-lotura**

Uneko estekari klase "aktibo" bat gehitzea, erabiltzaileak jakin dezan zein orrialdetan dagoen:

[src/nabigazio-barra-05.html](src/nabigazio-barra-05.html).

```HTML
<!doctype html>
<html>

<head>
  <style>
    ul {
      list-style-type: none;
      margin: 0;
      padding: 0;
      width: 200px;
      background-color: #f1f1f1;
    }

    li a {
      display: block;
      color: #000;
      padding: 8px 16px;
      text-decoration: none;
    }

    /* Change the link color on hover */
    li a:hover {
      background-color: #555;
      color: white;
    }

    .active {
      background-color: #04AA6D;
      color: white;
    }
  </style>
</head>

<body>

  <ul>
    <li><a class="active" href="#">Home</a></li>
    <li><a href="berriak.html">Berriak</a></li>
    <li><a href="kontaktua.html">Kontaktua</a></li>
    <li><a href="gu.html">Guri Buruz</a></li>
  </ul>

</body>

</html>
```

![](img/03.gif)

**Zentratu loturak eta gehitu ertzak**

[src/nabigazio-barra-06.html](src/nabigazio-barra-06.html).

```CSS
    ul {
      border: 1px solid #555;
    }

    li {
      text-align: center;
      border-bottom: 1px solid #555;
    }

    li:last-child {
      border-bottom: none;
    }
```

Gehitu `text-align:center` CSS kodea <li> edo <a> etiketei.

**Nabigazio barra bertikal finkoa, altuera osoan**

[src/nabigazio-barra-07.html](src/nabigazio-barra-07.html).

```HTML
<!doctype html>
<html>

<head>
  <style>
    ul {
      list-style-type: none;
      margin: 0;
      padding: 0;
      width: 25%;
      background-color: #f1f1f1;
      height: 100%;
      /* Full height */
      position: fixed;
      /* Make it stick, even on scroll */
      overflow: auto;
      /* Enable scrolling if the sidenav has too much content */
    }

    li a {
      display: block;
      color: #000;
      padding: 8px 16px;
      text-decoration: none;

    }

    /* Change the link color on hover */
    li a:hover {
      background-color: #555;
      color: white;
    }

    .active {
      background-color: #04AA6D;
      color: white;
    }
  </style>
</head>

<body>

  <h2>Zentratu loturak eta gehitu ertzak</h2>

  <ul>
    <li><a class="active" href="#">Home</a></li>
    <li><a href="#">Berriak</a></li>
    <li><a href="kontaktua.html">Kontaktua</a></li>
    <li><a href="gu.html">Guri Buruz</a></li>
  </ul>

  <div style="margin-left:25%;padding:1px 16px;height:1000px;">
    <h2>Fixed Full-height Side Nav</h2>
    <h3>Try to scroll this area, and see how the sidenav sticks to the page</h3>
    <p>Notice that this div element has a left margin of 25%. This is because the side navigation is set to 25% width.
      If you remove the margin, the sidenav will overlay/sit on top of this div.</p>
    <p>Also notice that we have set overflow:auto to sidenav. This will add a scrollbar when the sidenav is too long
      (for example if it has over 50 links inside of it).</p>
    <p>Some text..</p>
    <p>Some text..</p>
    <p>Some text..</p>
    <p>Some text..</p>
    <p>Some text..</p>
    <p>Some text..</p>
    <p>Some text..</p>
  </div>

</body>

</html>
```

![](img/04.gif)

[Posizioaren propietateak](https://www.w3schools.com/css/css_positioning.asp) zehazten du elementu batentzat erabiltzen den kokapen-metodo mota.

`position: fixed;` esan nahi du beti leku berean geratzen dela orria mugitu  arren. 

[src/fixed.html](src/fixed.html).

![](img/05.gif)


##  3. <a name='CSSNabigazio-barrahorizontala'></a>CSS Nabigazio-barra horizontala

Nabigazio-barra horizontal bat sortzeko bi modu daude. Zerrendako elementuetan `inline` edo `floating` erabilita.

###  3.1. <a name='Inlinezerrendaelementuak'></a>Inline zerrenda elementuak

[src/nabigazio-barra-08.html](src/nabigazio-barra-08.html).

```CSS
    li {
      display: inline;
    }
```

![](img/06.png)

Lehenespenez, `<li>` elementuak bloke-elementuak dira. Hemen, zerrendako elementu bakoitzaren aurretik eta ondoren linea-etendurak ezabatuko ditugu, lerro batean bistaratzeko.

* `display: inline;`: ez dute lerro etenaldirik sortzen beren aurretik edo ondoren. Fluxu normalean, hurrengo elementua lerro berean egongo da tarte bat badago.

###  3.2. <a name='Floatzerrendaelemantuak'></a>Float zerrenda elemantuak

```CSS
    li {
      float: left;
    }

    a {
      display: block;
      padding: 8px;
      background-color: #dddddd;
    }
```

![](img/07.png)

[src/nabigazio-barra-09.html](src/nabigazio-barra-09.html).

Adibidearen azalpenak:

[`float: left;`](https://developer.mozilla.org/es/docs/Web/CSS/float): Erabili _float_ blokeak bata bestearen ondoan kokatzeko. [MDN Web Docs - float](https://developer.mozilla.org/es/docs/Web/CSS/float).

[`display: block;`](https://developer.mozilla.org/es/docs/Web/CSS/display): Etiketa batzuk, lerro independenteetan azaltzen dura dira nabigatzailean, testuaren gainerakoarekin nahasi gabe, `<h1>` eta `<h6>` arteko izenburuak adibidez, `<p>` parrafoak, etab. Elementua hauek **bloke elementu** bezala ezagunak dira.

`background-color: #dddddd;` Fondo grisa `<a>` esteka etiketa bakoitzari.

###  3.3. <a name='Adibideak'></a>Adibideak

####  3.3.1. <a name='Fondoilunaestaestekenkoloreaaldatusaguagainetikpasatzean'></a>Fondo iluna esta esteken kolorea aldatu sagua gainetik pasatzean

[src/nabigazio-barra-10.html](src/nabigazio-barra-10.html).

```CSS
 ul {
      list-style-type: none;
      margin: 0;
      padding: 0;
      overflow: hidden;
      background-color: #333;
    }

    li {
      float: left;
    }

    li a {
      display: block;
      color: white;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;
    }

    /* Change the link color to #111 (black) on hover */
    li a:hover {
      background-color: #111;
    }
```

![](img/08.gif)

####  3.3.2. <a name='Aktibodagoenorrialdeaestekamarkatu'></a>Aktibo dagoen orrialdea / esteka markatu

[src/nabigazio-barra-11.html](src/nabigazio-barra-11.html).

```css
.active {
      background-color: #04AA6D;
    }
```

![](img/09.png)

####  3.3.3. <a name='Elementubateskubianlerrokatu'></a>Elementu bat eskubian lerrokatu

[src/nabigazio-barra-12.html](src/nabigazio-barra-12.html).

```HTML
  <ul>
    <li><a class="active" href="#">Home</a></li>
    <li><a href="#">Berriak</a></li>
    <li><a href="kontaktua.html">Kontaktua</a></li>
    <li style="float:right" ><a href="gu.html">Guri Buruz</a></li>
  </ul>
```

![](img/10.gif)

####  3.3.4. <a name='Ertzakeskubian'></a>Ertzak eskubian

[src/nabigazio-barra-13.html](src/nabigazio-barra-13.html).

```CSS
/* Add a gray right border to all list items, except the last item (last-child) */
li {
  border-right: 1px solid #bbb;
}

li:last-child {
  border-right: none;
}
```

![](img/11.png)



##  4. <a name='Kanpoestekak'></a>Kanpo estekak

* [https://www.w3schools.com/css/css_navbar.asp](https://www.w3schools.com/css/css_navbar.asp).
* [https://developer.mozilla.org/en-US/docs/Web/HTML/Element/li](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/li).
[https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ul](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ul).
* [https://www.w3schools.com/cssref/pr_class_display.asp](https://www.w3schools.com/cssref/pr_class_display.asp).
* [https://www.w3schools.com/css/css_positioning.asp](https://www.w3schools.com/css/css_positioning.asp)
* [https://developer.mozilla.org/en-US/docs/Web/CSS/display](https://developer.mozilla.org/en-US/docs/Web/CSS/display).

* [https://puzzleweb.ru/en/css/15_navbar.php](https://puzzleweb.ru/en/css/15_navbar.php).

* [https://www.washington.edu/accesscomputing/webd2/student/unit3/module6/lesson2.html](https://www.washington.edu/accesscomputing/webd2/student/unit3/module6/lesson2.html).
* [https://medialoot.com/blog/how-to-create-a-responsive-navigation-menu-using-only-css/](https://medialoot.com/blog/how-to-create-a-responsive-navigation-menu-using-only-css/).
* [https://dev.to/javascriptacademy/responsive-navigation-bar-with-mobile-menu-using-html-css-2hpd](https://dev.to/javascriptacademy/responsive-navigation-bar-with-mobile-menu-using-html-css-2hpd).




