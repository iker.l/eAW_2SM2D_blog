---
title: WordPress instalatu GNU/Linux sistema eragilean
subtitle: VBox + Mint 
date: 2021-10-31
draft: false
description: VBox + Mint 
tags: ["WordPress","Mint","Zerbitzaria","VBox","Virtual Box"]
---

<!-- vscode-markdown-toc -->
* 1. [Linux Mint deskargatu](#linux-mint-deskargatu)
* 2. [VBox-en sistema eragilea sortu](#vbox-en-sistema-eragilea-sortu)
* 3. [XAMPP](#xampp)
* 4. [Nola erabili XAMPP](#nola-erabili-xampp)
    * 4.1. [Komando-lerroen bitartez](#komando-lerroen-bitartez)
* 5. [Nola desinstalatu XAMPP](#nola-desinstalatu-xampp)
* 6. [WordPress-entzako Datu basea sortu phpMyAdmin](#wordpress-entzako-datu-basea-sortu-phpmyadmin)
* 7. [WordPress instalatu](#wordpress-instalatu)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='linux-mint-deskargatu'></a>Linux Mint deskargatu 

[https://www.osboxes.org/linux-mint/#linux-mint-20-2-vbox](https://www.osboxes.org/linux-mint/#linux-mint-20-2-vbox)

![](img/01.PNG)

* Username: osboxes
* Password: osboxes.org

##  2. <a name='vbox-en-sistema-eragilea-sortu'></a>VBox-en sistema eragilea sortu

Makina birtual berria sortu:

![](img/02.PNG)

![](img/03.PNG)

![](img/04.PNG)

Bukatu dugu, behin sortuta dagoenean makina berria abiarazi.

![](img/06.PNG)

##  3. <a name='xampp'></a>XAMPP

[https://www.apachefriends.org/es/download.html](https://www.apachefriends.org/es/download.html).

![](img/05.PNG)

Webgunetik deskargatu edo komandoen bitartez [wget](https://es.wikipedia.org/wiki/GNU_Wget) erabilita.

```bash
osboxes@osboxes:~/Downloads$ sudo wget https://www.apachefriends.org/xampp-files/7.3.31/xampp-linux-x64-7.3.31-3-installer.run
```

Fitxategia exekutatzeko baimenak eman: 

```bash
osboxes@osboxes:~/Downloads$ sudo -u root chmod +x xampp-linux-x64-7.3.31-3-installer.run
```

![](img/07.PNG)

Instalatzailea exekutatu administratzaile moduan (`sudo`):

```bash
osboxes@osboxes:~/Downloads$ sudo -u root ./xampp-linux-x64-7.3.31-3-installer.run
```

![](img/08.PNG)

![](img/09.PNG)

![](img/10.PNG)

![](img/11.PNG)

Instalazioa bukatutakoan XAMPP abiarazi:

![](img/12.PNG)

Ezinesteko zerbitzuak martxan jarri (Apache, MySQL).

![](img/13.PNG)

Gutxieneko frogak egin ondo doala ziurtatzeko, nabigatzailean zabaldu [http://localhost/](http://localhost/), barruan zaudenean kontsultatu PHPInfo eta phpMyAdmin atalak PHP eta datu basea eskuragarri daudela ziurtatzeko.

##  4. <a name='nola-erabili-xampp'></a>Nola erabili XAMPP 


###  4.1. <a name='komando-lerroen-bitartez'></a>Komando-lerroen bitartez

Zabaldu mahaigaineko _manager_-a:

```php
osboxes@osboxes:~/Downloads$ cd /opt/lampp/
osboxes@osboxes:~/opt/lampp$ sudo -u root chmod +x manager-linux-x64.run
osboxes@osboxes:~/opt/lampp$ sudo -u root ./manager-linux-x64.run
```

##  5. <a name='nola-desinstalatu-xampp'></a>Nola desinstalatu XAMPP

```php
osboxes@osboxes:~/opt/lampp$ sudo -u root ./uninstall
osboxes@osboxes:~/opt$ sudo -u root rm -r lampp
```

##  6. <a name='wordpress-entzako-datu-basea-sortu-phpmyadmin'></a>WordPress-entzako Datu basea sortu phpMyAdmin


##  7. <a name='wordpress-instalatu'></a>WordPress instalatu

sistema eragilea edozein dela ere, jarraitu beharreko urratsak berdinak dira instalaziorako.

Deskargatu azken bertsioa hemendik: [https://es.wordpress.org/downloads/](https://es.wordpress.org/downloads/).

```bash
osboxes@osboxes:~/opt/lampp$ cd htdocs
osboxes@osboxes:~/opt/lampp/htdocs$ sudo -u root wget https://es.wordpress.org/latest-es_ES.zip
osboxes@osboxes:~/opt/lampp/htdocs$ sudo -u root unzip latest-es_ES.zip
```

Orain instalazioa hasi dadin zabaldu nabigatzailean [http://localhost/wordpress](http://localhost/wordpress).


