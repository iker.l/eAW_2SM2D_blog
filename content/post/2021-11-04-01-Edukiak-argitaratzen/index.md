---
title: Edukiak argitaratzen
subtitle: WordPress hastapenak
date: 2021-11-04
draft: false
description: WordPress hastapenak
tags: ["WordPress","WP","WordPress - Hastapena","WordPress - Edukiak","WordPress - Bidalketak"]
---

WP instalatu duzu dagoeneko, prest gaude edukiak gehitzeko. Atal honetan, topiko hauei buruz ikasiko duzu:

* Nola idatzi _post_ berri bat.
* Nola kontrolatu _post_ bati lotutako informazioa, izenburua eta edukia ez ezik, irudiak eta multimedia-fitxategiak ere.
* Iruzkinak, zertarako eta nola kudeatu.
* Eduki antolatua eta erraz bilatzeko modukoa izatea, etiketak eta kategoriak erabiliz.

![](img/01.png)

## Posten zerrenda

Oraindik gogoratzen duzu zein ziren mahaigaineko pantaila-aukerak?

Zerrendako sarrera bakoitzak **editatzeko klikatu daitekeen argitalpenaren izenburua du**. Saguaren erakuslea tituluaren gainetik mugitzen baduzu, editatzeko, ikusteko edo ezabatzeko aukera berriak ikusiko dituzu.

![](img/02.PNG)

Autorearen gainean klik egiten badugu, bereak bakarrik erakutsiko dizkigu. Kategorien eta etiketen arabera ere iragaz dezakegu. Artikuluaren iruzkin-kopurua adierazten du baita.

## Edukiak argitaratzen

Bloga sortzen ari bagara, jarduera nagusia bidalketak (_post_) dira, aldizkari bateko artikulu baten gisakoak, izenburu, gorputz edo eduki bat eta autore bat behar ditu (hainbat rol dituzten erabiltzaileak onartzen dituen wp-erabiltzailea). Post batekin batera, informazio osagarri pila bat dator, hala nola data, etiketak eta kategoriak.

### Post erraz bat gehitu

Bidalketa bat gehitzeko bi modu ditugu:

1. Joan **Bidalketa | Berria gehitu** atalera menu nagusitik.
2. Egin klik "Berria gehitu" botoiaren gainean sarreren zerrendan.

### Bloke-editorearekin lanean

Blokeen editore bisuala azken berrikuntzen onenetako bat da (ez dute denek berdin pentsatzen), [5.0 bertsioan](https://wordpress.org/support/wordpress-version/version-5-0/#list-of-files-revised) (Bebo) lehenespenez sartu zen. Editore berriak [**Gutenberg**](https://es.wordpress.org/plugins/gutenberg/) izena du ([Johannes Gutenberg](https://es.wikipedia.org/wiki/Johannes_Gutenberg) inprentaren asmatzaileari dagokionez).

![](img/03.PNG)

Aurreko kaptura bloke-editorearen ikuspegi nagusia da. Hala deitzen zaio, posteko elementu bakoitza bloke bat delako (paragrafo bat, irudi bat, taula bat, etab.). Alde handia dago [WYSIWYG](https://es.wikipedia.org/wiki/WYSIWYG) editore zaharragoaren eta LibreOffice edo MS Worddokumentu-editore baten artean:

![](img/04.png)

Bloke-editore berriak zentzu handiagoa du web-arentzat, elementu bakoitzak bere aldetik funtzionatzen baitu; adibidez, gailu eta pantaila desbderdinei egokitu ahal izateko (_responsive_), errazagoa da blokeak berrantolatzea testu-paragrafoak edo eduki jarraitua baino.

### Edizio-interfazea ulertuz

![](img/05.png)

Editoreak hiru atal nagusi ditu:

1. Edukiak editatzeko atal nagusia.
2. Alboko doikuntza-barra. Blokeen edo dokumentu osoaren doikuntza “finagoak” egiteko aukera ematen du.
3. Goiko menua.

Goiko menu-barraren ikuspegi zehatzagoa:

![](img/06.png)

Ikurrak ezkerretik hasita:

* + ikonoak bloke berri bat gehitzeko aukera ematen du.
* Blokeak editatzeko / hautatzeko arkatza.
* Desegin edo berregin aldaketak.
* Dokumentuaren informazioa (hitzak, goiburuak, paragrafoak, etab.).
* Aurrebista (gailuen dimentsio desberdinetan).
* Argitalpena.
* Alboko doikuntza-barra ezkutatzeko gurpil koskaduna.
* Hiru puntu bertikaleko menuak editoreko elementuak gehitu edo kentzeko aukera ematen du (kode-editorea edo ikusizko editorea).

### Zure lehen post-a sortzen

Bada garaia gure lehen post hau hasteko!

Hasteko, izenburu bat:

![](img/07.png)

Definitu duzunean, mugitu kurtsorea beherantz eta hasi idazten.

![](img/08.png)

Idazten hasi ahala, hainbat edizio-tresna dituzula ikusiko duzu. Testua ezkerrean, erdian edo eskuinean lerrokatu dezakezu. Letra lodiz edo etzanez jar dezakezu, edo esteka bat gehitu. Zerrenda bat gehitzeko, sartu _Enter_ eta hasi hurrengo lerroa gidoi batekin.

Bloke bat ezabatzeko edo beste ekintzaren bat egiteko, hiru puntuko menua erabil dezakezu.

![](img/09.png)

Horretaz gain, bloke bat editatzen ari zarenean, bloke espezifikoa konfiguratzeko aukerak ikus daitezke. Adibidez, paragrafo baterako iturria doitu dezakezu, hasierako letra handia erakutsiz kapitalizatu, etab.

![](img/10.png)

Orain, + ikonoa duen irudia gehituko dugu.

![](img/11.png)

Irudi-blokeak irudi berri bat igotzeko, multimedia-galeriatik edo URL batetik hautatzeko aukera ematen du.

### Blokeak

#### Zutabeak

Bloke-editorearen gauzetako bat aldakortasuna eta funtzionaltasuna da. Oso erraza da elementuak mugitzea eta haien posizioa doitzea, perfektuak izan daitezen.

Bloke bat bestearen azpian jartzea txanponaren alde bat besterik ez da. Gora eta behera arrastatu eta askatu edo mugitu daitezke geziak erabiliz. Beste gauza bat egin dezakegu: bloke jakin baten aurretik edo ondoren beste bloke bat gehitu.

[Blokeak zutabetan lerrokatzeko](https://wordpress.com/es/support/editor-wordpress/bloques/bloque-de-columnas/), zutabeetarako bloke zehatz bat erabiltzen dugu; gehienez ere sei blokek lauki-efektua sortzen dute.

![](img/12.gif)

Zutabe-kopurua definitutakoan, horietako bakoitzari edukia gehitu ahal izango diozu. Zutabe-blokearen alderdirik zoragarriena da zutabeen barruan beste bloke batzuk gehitu ditzakezula.

Adibidez, zutabe-blokea egitura gisa erabil dezakezu, eta irudi-blokeak, goiburu-blokeak eta paragrafo-blokeak gehitu, zerbitzuekin lauki-sare bat sortzeko.

![](img/13.png)

Aurreratua fitxaren bidez, **CSS klase bat gehi diezaiokezu zure blokeari**. Hala, CSS kode pertsonalizatua idatz dezakezu eta nahi duzun estiloa eman.

![](img/14.png)

### Argitalpenen aukera aurreratuak

#### Laburpenak

![](img/15.png)

## Baliabideak

* [https://wordpress.com/es/support/editor-wordpress/bloques/bloque-de-columnas/](https://wordpress.com/es/support/editor-wordpress/bloques/bloque-de-columnas/).
* [https://wordpress.com/es/support/editor-wordpress/#adding-a-block](https://wordpress.com/es/support/editor-wordpress/#adding-a-block).


