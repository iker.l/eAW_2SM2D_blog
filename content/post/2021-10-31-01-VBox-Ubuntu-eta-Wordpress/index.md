---
title: WordPress instalatu GNU/Linux sistema eragilean
subtitle: VBox + Ubuntu 
date: 2021-10-31
draft: true
description: VBox + Ubuntu 
tags: ["WordPress","Ubuntu","Zerbitzaria","VBox","Virtual Box"]
---

# ERRAMINTAK

Deskargatu behar dituzunak, Virtual Box birtualizazio aplikazioa eta Ubuntu sistema eragilea.

* [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads).
* [https://www.osboxes.org/ubuntu/](https://www.osboxes.org/ubuntu/). Ubuntu 21.04 Hirsute Hippo deskargatu. Username: osboxes, Password: osboxes.org.

## VBOX ETA UBUNTU

VBox instalatu eta zabaldu:

![](img/01.PNG)

Ezarpenetan adierazi non gordeko dituzun birtualizatutako irudiak:

![](img/02.PNG)

Bertara kopiatu deskomprimitutako irudia "Ubuntu 21.04 (64bit).vdi" 

Orain makina birtual berria sortuko dugu VBox-en.

![](img/03.PNG)

![](img/04.PNG)

Ezarri gutxieneko RAM memoria.

![](img/05.PNG)

Aukeratu deskargatu dugun disko gogorraren irudia Ubuntu sistema eragilearekin:

![](img/06.PNG)

Bukatu dugu! 

![](img/07.PNG)

Orain makina birtuala abiarazi.

![](img/08.PNG)

## HTTP ZERBITZARIA INSTALATU




