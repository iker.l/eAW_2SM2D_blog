---
title: HTML orridalde baten oinarrizko egitura
subtitle: Oinarrizko HTML
date: 2021-11-27
draft: false
description: Oinarrizko HTML
tags: ["HTML","HTML - Orrialdeak","HTML - Egitura"]
---

<!-- vscode-markdown-toc -->
* 1. [HTML Etiketak](#HTMLEtiketak)
	* 1.1. [Elementu hutsak](#Elementuhutsak)
	* 1.2. [Etiketak etiketen barruan](#Etiketaketiketenbarruan)
	* 1.3. [Etiketen parametroak edo atributuak](#Etiketenparametroakedoatributuak)
* 2. [Orri baten oinarrizko egitura](#Orribatenoinarrizkoegitura)
* 3. [Web orria frogatzen](#Weborriafrogatzen)
	* 3.1. [Zelan kargatu orrialdea VSCode editorea erabilita](#ZelankargatuorrialdeaVSCodeeditoreaerabilita)
		* 3.1.1. [HTML Preview](#HTMLPreview)
		* 3.1.2. [Live Server](#LiveServer)
		* 3.1.3. [open in browser](#openinbrowser)
		* 3.1.4. [VSCode beste luzagarri batzuk](#VSCodebesteluzagarribatzuk)
* 4. [Baliabideak](#Baliabideak)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Web orri bat sortzeko oinarrizko elementuak, **etiketak** eta **egiturazko diseinua** ulertzen hasi behar da. Modulu honetan bi kontzeptuak aztertuko ditugu eta gure lehen adibideak sortuko ditugu.

##  1. <a name='HTMLEtiketak'></a>HTML Etiketak

[https://jsfiddle.net/ikerlandajuela/t6qoz8u0/latest](https://jsfiddle.net/ikerlandajuela/t6qoz8u0/latest). 

![](img/01.png)

Gure web orriak sortzen hasteko egin beharko dugun gauza bakarra, dokumentu baten barruan html etiketak jartzea izango da.

HTML etiketa bat txikiago eta handiago zeinuekin inguratutako termino bat besterik ez da; adibidez:

```html
<strong>
```

Etiketa honen arabera, jarraian agertzen den testua nabarmendu behar da; normalean, letra lodiz.

Kasu askotan, **bigarren etiketa bat beharko dugu, amaiera mugatzeko**. Horrela, testua nabarmenduta jarraitu ez dezan, eta modu normalean
jarraitu nahi badugu, etiketa bera erabiltzen dugu barra batekin:

```html
<strong>letra lodiz agertuko naiz</strong>
```

Bi etiketek eta barruko testuek **HTML elementu** bat osatzen dute.

###  1.1. <a name='Elementuhutsak'></a>Elementu hutsak

![](img/07.png)

**Elementu batzuek ez dute edukirik** eta elementu hutsak esaten zaie. Elementu batzuk ireki eta itxi behar dira, eta beste batzuetan, berriz, etiketa bakarra beharko dugu, elementu jakin bat edo eduki hutsaren bat agertzen baita. Adibidez:

```html
<hr/>
```

Dokumentuan lerro bat agertzen da, hurrengoaren aurreko testua bereiziz. Ikus daitekeenez, elementu batek ez badu itxiera-etiketarik (ez da
`</hr>` existitzen), amaierako adierazlea hasierako etiketan bertan erantsi behar da.

**Oharra**: html etiketa guztiak **letra xehetan** idazten dira beti. Nahiz eta nabigatzaileak letra larriz interpretatzeko gai diren, W3C-ren arauek zehazten dute etiketa guztiak letra xehean idatzi behar direla.

###  1.2. <a name='Etiketaketiketenbarruan'></a>Etiketak etiketen barruan

Etiketen beste ezaugarri garrantzitsu bat da anidableak direla, eta, beraz, lerrokada (parrafo) bat defini dezakegu (`<p>` eta `</p>` etiketen bidez), eta barruan elementu, irudi, testu nabarmen eta abarren zerrenda bat, etiketak erabiliz. Adibide asko ikusiko ditugu.

```html
<p>Nire katua <strong>oso</strong> handia da.</p>
```

###  1.3. <a name='Etiketenparametroakedoatributuak'></a>Etiketen parametroak edo atributuak

Html etiketa askok behar bezala funtzionatzeko parametroak behar dituzte. Horiek irekitze eta itxiera-zeinuen artean sartzen dira, html elementuaren ñabardura zehatzen bat definitzeko.

![](img/02.png)

Adibidez, `<img/>` etiketa, web orrian irudi bat txertatzeko erabiltzen da, baina ez du ongi funtzionatzen. Parametro bat jarri behar duzu, non adieraziko dugun zein irudi ikusiko den.

Honela geratuko litzateke:

```html
<img src="https://fptxurdinaga.hezkuntza.net/image/layout_set_logo?img_id=137406&t=1637988685233" />
```

Hurrengo adibidean, zein irudi erakutsiko den adierazteaz gain, pantailan hartuko duen tamaina ezarriko dugu:

```html
<img src="https://fptxurdinaga.hezkuntza.net/image/layout_set_logo?img_id=137406&t=1637988685233" width="62" height="32" />
```

Parametroak termino baten bidez identifikatzen dira, ondoren berdin zeinu bat, eta ondoren, komatxoen artean, esleitu nahi diogun balioa.

**Oharra**: nahiz eta komatxo sinpleak erabil ditzakegun, normalean binakako komatxo bikoitzak erabiltzen dira parametro bakoitzaren balioa ezartzeko.

##  2. <a name='Orribatenoinarrizkoegitura'></a>Orri baten oinarrizko egitura

* [https://jsfiddle.net/ikerlandajuela/jt46szaL/latest](https://jsfiddle.net/ikerlandajuela/jt46szaL/latest)
* [src/HTML_Oinarrizko_orri_egitura.html](src/HTML_Oinarrizko_orri_egitura.html)

Web orriek egitura oso sinplea dute, eta **errespetatu egin behar dugu**, nabigatzaileak aurkezteko gai izan daitezen. Adibidez ezin dugu gure orrialdea lerrokada (parrafo) etiketarekin hasi, zein orrialde mota sortzen ari garen adierazi behar dugu, zer informazio osagarri izango dugun eta non hasi behar den azaldu.

Horrela, edozein web orrian honako etiketa hauek sartuko dira gutxienez:

* `<html>` eta `</html>` dokumentuaren hasieran eta amaieran jarrita, web **orria non hasi eta bukatzen den adierazten dute**.
* `<head>` eta `</head>` espazio bat definitzen dute, eta bertan, nabigatzailean zuzenean agertuko ez diren edukiak sartuko ditugu, **dokumentuaren zenbait alderdi deskribatzeko** balio dutenak, hala nola, izenburua, egilea, erabiliko ditugun estiloak, funtzio txikiak eta abar.
* `<body>` eta `</body>` etiketen barruan, **nabigatzailean erakutsiko den informazioa** sartzen da. Orrialdearen egiazko edukia da, etiketen bidez egituratua.

Aurreko hiru etiketekin batera, beste etiketa garrantzitsu batzuk ere aurki ditzakegu web orria zuzen interpretatzeko. `<!DOCTYPE>` zein dokumentu mota sortzen ari garen eta zein araura egokitzen diren adierazten dutenak.

Html bertsioari buruzko informaziorik ez duen dokumentu bat, normalean inolako arazorik gabe interpretatuko du nabigatzaileak, baina dokumentua erabat zuzena izatea nahi bada agertu beharko da.

Nabigatzaileek aurkeztu behar duten dokumentuaren HTML bertsio jakin dezaten beharrezkoa `<!DOCTYPE>` jartzea.

HTML5-ean DOCTYPE deklarazioa sinplifikatu egin da. Izan ere, bakarra da. Nabigatzaileak HTML5 zehaztapena erabiliz web orria pantailan
erakusteko, dokumentuan gehitu beharreko DOCTYPE-a da:

```html
<!DOCTYPE html>
```

DOCTYPE html adierazpena html dokumentu baten lehen elementua izan behar da, `<html>` baino lehen.

Dokumentuaren goiburuak, `head` elementuak mugatuta, web orriari buruzko informazioa jasotzen du: izenburua, metadatuak, estiloak... Dokumentuaren edukiari buruzko informazioa eta, oro har, dokumentuaren edukiaren zati ez diren datuak, baina horri buruzko informazioa ematen dute. Burualdearen balizko osagaietatik, garrantzitsuena eta nahitaezkoa da dokumentuaren izenburua (`<title>` elementua).

`<tittle>` elementuak ezin du beste elementu batzuk eduki, ezta formatu-adierazpenik ere.

**Oharra**: ez da nahastu behar dokumentu osoan eragina duen title elementua, hainbat elementuri eragiten dien title atributuarekin.

Head elementua, title izateaz gain, honako elementu hauek izan ditzake: meta, link, base, isindex, style eta script.

Gorputza `<body>` elementuarekin edo `<frameset>` mugatzen da, markoak dituen dokumentua bada.

Aurreko guztiarekin, zerotik sortutako web orri sinple bat honela geratuko litzateke:

```html
<!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8" >
            <title>Orrialdearen izenburua</title>
        </head>

        <body>

            <h1>Goi mailako izenburua</h1>
            <p>Lerrokada.</p>

        </body>
    </html> 
```

Hau da guztia. Badugu gure lehen web orria diseinatuta.

##  3. <a name='Weborriafrogatzen'></a>Web orria frogatzen

Bakoitza aurreko web orria frogatzen saiatuko da hurrengo pausoak jarraituz.

[Visual Studio Code](https://code.visualstudio.com/) editorea irekiko dugu, barruan, aurreko testua kopiatu eta itsatsiko dugu, edo zerotik idatziko dugu. Bigarren kasu honetan, arreta handia jarri behar dugu, etiketa gaizki ez idazteko.

Jarraian, gure ordenagailuaren karpeta batean gordeko dugu web orria. **Artxiboaren izena .html edo .htm luzapena izan behar du**. Horrela,
index.html izena erabil dezakegu, hau izaten da gehienetan web orri nagusiari esleitzen zaiona. 

**Oharra**: web orrietako artxiboen izenak letra xehez idatzi beharko lirateke beti, espaziorik gabe eta karaktere berezirik gabe, letra eta zenbakietara murriztuz eta, badaezpada, gidoi altua edo baxua. Horrela ez dugu arazorik izango gure orriak web zerbitzari batera igotzeko.

Gure artxibo-esploratzailea erabiliz, karpetara sartuko gara; artxiboa ikusi behar dugu, eta haren gainean klik bikoitza egin ahal izango dugu, gure nabigatzailearen barruan zabal dadin.

###  3.1. <a name='ZelankargatuorrialdeaVSCodeeditoreaerabilita'></a>Zelan kargatu orrialdea VSCode editorea erabilita

####  3.1.1. <a name='HTMLPreview'></a>HTML Preview

Instalatu [HTML Preview](https://marketplace.visualstudio.com/items?itemName=tht13.html-preview-vscode) luzapena [VSCode](https://code.visualstudio.com/) editorean:

![](img/03.png)

Erabili goiko eskumako aldean dagoen ikono berria edota `ctrl+shift+v` teklatuaren lasterbidea zure lehen web orria nola gelditzen den ikusteko.

####  3.1.2. <a name='LiveServer'></a>Live Server

[Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)-ek serbitzari bat abiarazten du zure ordenagailu lokalean, egiten dituzun aldaketak zure orrian dinamikoki kargatzen dira serbitzarian gordetzen dituzunean.

![](img/04.png)

Serbitzaria abiarazteko hainbat modu dituzu, adibidez editore barruan HTML fitxategian zaudenean saguaren eskumako klik eginez aukeratu menuan "Open with Live Server", edota laster teklak erabilita "ALT+L ALT+O". Gelditzeko ere menutik edota "ALT+L ALT+C" tekla jokoak erabilita. Komandoen aurkibidean ere topatuko duzu "ctrl+shift+P".

####  3.1.3. <a name='openinbrowser'></a>open in browser

![](img/06.png)

[open in browser](https://marketplace.visualstudio.com/items?itemName=techer.open-in-browser) Izenak dioen bezala nabigatzailean zabaltzen du.

* "Alt + B" lehenetsitako nabigatzailean irekitzeko.
* "Shift + Alt + B" nabigatzailea aukeratzeko.

Behin zabalduta HTML fitxategian egiten dituzun aldaketak ez dira automatikoki eguneratzen nabigatzailean, eskuz eguneratu beharko duzu (F5).

####  3.1.4. <a name='VSCodebesteluzagarribatzuk'></a>VSCode beste luzagarri batzuk

Bide batez beste luzagarri batzuk instalatu ditzakezu HTML-rekin lan errezago egiteko:

[Highlight Matching Tag](https://marketplace.visualstudio.com/items?itemName=vincaslt.highlight-matching-tag)

![](img/05.png)

[Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode), formatua ematen dio HTML kodeari.

1. CMD + Shift + P -> Format Document

Edo

1. Aukeratu testu zati bat.
2. CMD + Shift + P -> Format Selection

[Beautify](https://marketplace.visualstudio.com/items?itemName=HookyQR.beautify). CTRL + Mayus + P eta "Beautify file".

##  4. <a name='Baliabideak'></a>Baliabideak

* MDN Web Docs [HTML basics](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/HTML_basics).
* W3schools - HTML Introduction [https://www.w3schools.com/html/html_intro.asp](https://www.w3schools.com/html/html_intro.asp).

HTML etiketak:

* W3schools [HTML <strong> Tag](https://www.w3schools.com/tags/tag_strong.asp)
* MDN Web Docs [<strong>: The Strong Importance element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/strong).
