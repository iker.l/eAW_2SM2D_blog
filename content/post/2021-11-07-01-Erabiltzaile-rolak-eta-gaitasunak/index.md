---
title: Erabiltzaile rolak eta gaitasunak
subtitle: WordPress admin hastapenak
date: 2021-11-07
draft: false
description: WordPress admin hastapenak
tags: ["WordPress","WP","WordPress - Hastapena","WordPress - Erabiltzaileak"]
---

<!-- vscode-markdown-toc -->
* 1. [Erabiltzaile rolak](#Erabiltzailerolak)
	* 1.1. [Administraria](#Administraria)
	* 1.2. [Editorea](#Editorea)
	* 1.3. [Autorea](#Autorea)
	* 1.4. [Laguntzailea](#Laguntzailea)
	* 1.5. [Harpideduna](#Harpideduna)
* 2. [Erabiltzaileak kudeatu](#Erabiltzaileakkudeatu)
* 3. [Utzi erabiltzaileei erregistratzen](#Utzierabiltzaileeierregistratzen)
* 4. [Baliabideak](#Baliabideak)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Erabiltzailerolak'></a>Erabiltzaile rolak

WordPress-ek zure web gunean erabiltzaile kopuru mugagabe batekin lan egiteko aukera ematen dizu. Erabiltzaile bakoitzak bere rolak izan ditzake. Ikusi ditzagun banan banan.

![](img/02.png)



###  1.1. <a name='Administraria'></a>Administraria

WordPress instalatzen duzunean, automatikoki sortzen da administrari erabiltzaile bat (WordPress-ek beti izan behar du gutxienez rol hau duen kontu bat), konturatuko zinan administrariak edozer egin dezaketela.

Orokorrean ez dituzu nahi izango administrari rola duten erabiltzaile asko wegbune baten, hobeto da bakarra izatea ahal den neurrian.

Administratzaile gehiago sortzean (edo horietako bat kudeatzean), saia zaitez pasahitz konplexuak soilik erabiltzen, eta hori oso garrantzitsua da _brute-force_ erasoak ekiditeko.

###  1.2. <a name='Editorea'></a>Editorea

Administratzailearen ondoren, editoreek dute gauzak egiteko baimen gehien. Rol honek Web gunearen edozein eduki aldatu dezakete, baina ez oinarrizko egitura, diseinua edo funtzionaltasuna (hori administratzaileentzat erreserbatuta dago). 

**Editorearen helburu nagusia web gunearen edukia aldatzea da**.

Editore baten manu nagusian agerikoa da aukera gutxiago daudela, agertzen ez diren zenbait: _plugin_, itxura (_widget_, menuak, _theme_), erabiltzaileak,etab.

![](img/03.png)


Editore-rolarekin batera erabil daitezkeen jardueren adibideak hauek dira:

* Bidalketa guztiak kudeatu.
* Orrialde berriak sortu eta editatu.
* Iruzkinak moderatu.
* Kategoriak, etiketak eta estekak kudeatu.
* Editatu beste erabiltzaile batzuek sortutako edukinak.

###  1.3. <a name='Autorea'></a>Autorea

Autoreek editoreek baino sarbide-maila txikiagoa dute. Autoreek bidalketa berriak sortu ahal dituzte eta berainek direnak editatu, lehen helburua haien edukiak kudeatzea da.

Autoreek menu hau ikusten dute:

![](img/04.png)

Bidaleta guztiak ikusi ditzazkete baina bakarrik beraienak editatu:

![](img/05.png)

###  1.4. <a name='Laguntzailea'></a>Laguntzailea

Laguntzaileek bidalketa berriak sortu ahal dituzte eta beste erabiltzaile batek berrikusteko moduan utzi argitaratu gabe, bidalketa hauek administrari edo editore batek berriskuteko eta argitaratzeko zain geldituko dira.

![](img/06.png)

###  1.5. <a name='Harpideduna'></a>Harpideduna

Harpidedunek ezin dute ezer egin, saioa hasi eta profila editatu besterik ezin dute egin (izena, abizena, pasahitza, informazio biologikoa eta abar doitu). ), eta hori da guztia.

Doikuntzetan eta eztabaidan ezarritako baimenen arabera (**Ezarpenak | Iruzkinak**), blogeko bisitariek harpidedun gisa erregistratu behar izaten dute iruzkinak argitaratzeko.

![](img/07.png)


##  2. <a name='Erabiltzaileakkudeatu'></a>Erabiltzaileak kudeatu

Erabiltzaileak administratzeko, hasi saioa (administratzaile gisa, jakina) eta joan **Erabiltzaileak** atalera. Zure erabiltzaileen zerrenda ikusiko duzu, argazki honetan ikusten den bezala:

![](img/08.png)

Beraz, erabiltzaile berri bat sortuko dugu, eta "Editore" rola esleituko diogu erabiltzaileari (baimen gehien dituena administrariaren ondoren).

![](img/09.png)


Formulario honetan, erabiltzaile-izenaren eta posta elektronikoaren eremuak baino ez dira behar. Pasahitza automatikoki sortzen da. WordPress-ek berez pasahitz segurua sortuko du, eta, gero, erabiltzaile berriari bidaliko dio, egin ondoren. Aurrez zehaztutako baliotik (Harpideduna) beste roletako batera ere alda dezakezu Rola. Kasu honetan, editorea hautatu dut.

Ondoren, egin klik Gehitu erabiltzaile berria botoian. Eskatutako eremuez gain, komeni da Izena eta Abizena betetzea, aurrerago erraztu egiten du erabiltzaileak kudeatzea.

##  3. <a name='Utzierabiltzaileeierregistratzen'></a>Utzi erabiltzaileei erregistratzen

Erabiltzaileak gehitzea ez da WordPress-en webgunera erabiltzaileak gehitzeko modu bakarra. Beraien kontura erregistratzeko gaitasuna ere eman diezaiekezu erabiltzaileei, **"Ezarpenak | Orokorra"** atalean dago auekera.

![](img/10.png)


##  4. <a name='Baliabideak'></a>Baliabideak

* [https://wordpress.org/support/article/roles-and-capabilities/](https://wordpress.org/support/article/roles-and-capabilities/).


