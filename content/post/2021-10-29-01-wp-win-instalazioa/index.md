---
title: WordPress instalazioa Windows sistema eragilean
subtitle: WordPress eta XAMPP zerbitzari moduan
date: 2021-10-29
draft: false
description: WordPress eta XAMPP zerbitzari moduan
tags: ["WordPress","WP","Instalazioa","Windows","XAMPP"]
---

## WORDPRESS NOLA INSTALATU

### Datu basea sortu phpMyAdmin erabilita

XAMPP zerbitzaria ondo instalatu badugu hurrengo pausua datu base bat sortzea da WordPress-ek erabili dezan, zabaldu esteka hau: [http://localhost/phpmyadmin](http://localhost/phpmyadmin). 

#### Erabiltzaile kontu berri bat sortu

"root" kontua erabili dezakegu (pasahitzik gabe) edo erabiltzaile propio bat sortu WordPress-entzako bereziki (segurtasunaren aldetik egokiagoa).

![](img/03.PNG)

Ondoren, erabiltzailearen datuak sartuko ditugu, izen bat eta pasahitz bat esleituz. "Zerbitzaria" eremuan, "Lokala" hautatuko dugu, "Localhost" jar diezagun. Pantailaren behealdean, "Sortu izen bereko datu-basea eta eman pribilegio guztiak" hautatuko dugu, eta, ondoren, "Sortu erabiltzailea" sakatuko dugu.

![](img/04.PNG)

Dena ondo joan bada, Erabiltzailearen sorreraren eta datu-basearen mezu bat izango dugu. Ondo gogoratu datu-basea sortzeko sartu dituzun datuak, instalaziorako beharrezkoa izango baita.

### WordPress instalazioa

Zerbitzari lokala instalatu eta datu-basea sortu ondoren, Wordpress kopia bat baino ez zaigu geratzen instalatzeko. [https://es.wordpress.org/download/](https://es.wordpress.org/download/) helbidera jo behar dugu erdarazko bertsio bat jaisteko.  Tutorial hau idazteko unean 5.8.1 bertsioa dago eskuragarri.

![](img/01.png)

Orain konprimitutako fitxategia daukagu gure konputagailuan (wordpress-5.8.1-es_ES.zip), Wordpress instalatzeko behar diren fitxategi guztiak dituena. Fitxategi horiek guztiak gure zerbitzari lokaleko karpeta publikoan deskonprimatu behar ditugu, gure disko gogorrean kokatuta baitago, eta izen desberdinak izan ditzake, hala nola "htdocs" edo "www" (C:\xampp\htdocs\). 

![](img/05.PNG)

Jarraian, nabigatzaile bat irekiko dugu (Firefox,Chrome,Edge...) eta [http://localhost/wordpress](http://localhost/wordpress) tekleatuko dugu helbide-barran, instalazioarekin hasteko.

Hurrengo pantailan, Wordpressek adierazten digu zer datu behar ditugun instalatzeko.

![](img/06.PNG)

![](img/07.PNG)

Nabigatzailetik instalazioarekin jarraitzeko, "Hasi instalazioa" botoia sakatuz.

![](img/08.PNG)


Jarraian, Wordpress-eko erabiltzaileen gure izena adieraziko dugu, besterik adierazi ezean admin dela, pasahitz bat jarriko dugu (rFthVxiCaWN!zO(9qy), posta elektronikoko helbide bat,

Eta aukeratu bai nahi dugu edo ez bilaketa motorren bidez (Google, adibidez); eta jarraitu.

![](img/09.PNG)

Azken pantaila horrekin, Wordpressek ez du jakinarazi instalazioa arrakastaz amaitu duenik.

![](img/10.PNG)

[http://localhost/wordpress/wp-login.php](http://localhost/wordpress/wp-login.php)

## Baliabideak

* [How to install WordPress](https://wordpress.org/support/article/how-to-install-wordpress/).

